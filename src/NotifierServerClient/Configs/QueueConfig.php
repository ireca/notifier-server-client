<?php
namespace NotifierServerClient\Configs;

class QueueConfig extends AbstractBaseConfig
{
    /**
     * @var string
     */
    private $routingKey;

    /**
     * @var array
     */
    private $headers = array();

    /**
     * @param string $routingKey
     * @param array $headers
     */
    public function __construct($routingKey, array $headers)
    {
        $this->routingKey = $routingKey;
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getRoutingKey()
    {
        return $this->routingKey;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

}