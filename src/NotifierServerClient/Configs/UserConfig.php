<?php
namespace NotifierServerClient\Configs;

class UserConfig extends AbstractBaseConfig
{
    /**
     * @var string
     */
    private $user = '';

    /**
     * @var string
     */
    private $users = '';

    /**
     * @var string
     */
    private $create = '';

    /**
     * @var string
     */
    private $update = '';

    /**
     * @var string
     */
    private $delete = '';

    /**
     * @param string $user
     * @param string $users
     * @param string $create
     * @param string $update
     * @param string $delete
     */
    public function __construct($user, $users, $create, $update, $delete)
    {
        $this->user = $user;
        $this->users = $users;
        $this->create = $create;
        $this->update = $update;
        $this->delete = $delete;
    }

    /**
     * @return string
     */
    public function getUserUrl()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getUsersUrl()
    {
        return $this->users;
    }

    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->create;
    }

    /**
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->update;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->delete;
    }
}
