<?php
namespace NotifierServerClient\Configs;

class MessagesConfig extends AbstractBaseConfig
{
    /**
     * @var string
     */
    private $message = '';

    /**
     * @var string
     */
    private $create = '';

    /**
     * @var string
     */
    private $instantlyCreate = '';

    /**
     * @var string
     */
    private $delete = '';

    public function __construct($message, $create, $instantlyCreate, $delete)
    {
        $this->message = $message;
        $this->create = $create;
        $this->instantlyCreate = $instantlyCreate;
        $this->delete = $delete;
    }

    /**
     * @return string
     */
    public function getMessageUrl()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->create;
    }

    /**
     * @return string
     */
    public function getInstantlyCreateUrl()
    {
        return $this->instantlyCreate;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->delete;
    }
}
