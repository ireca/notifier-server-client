<?php
namespace NotifierServerClient\Configs;

class DefaultSettingsConfig extends AbstractBaseConfig
{
    /**
     * @var string
     */
    private $setting = '';

    /**
     * @var string
     */
    private $settings = '';

    /**
     * @var string
     */
    private $create = '';

    /**
     * @var string
     */
    private $update = '';

    /**
     * @var string
     */
    private $delete = '';

    /**
     * @param string $setting
     * @param string $settings
     * @param string $create
     * @param string $update
     * @param string $delete
     */
    public function __construct($setting, $settings, $create, $update, $delete)
    {
        $this->setting = $setting;
        $this->settings = $settings;
        $this->create = $create;
        $this->update = $update;
        $this->delete = $delete;

    }

    /**
     * @return string
     */
    public function getSettingUrl()
    {
        return $this->setting;
    }

    /**
     * @return string
     */
    public function getSettingsUrl()
    {
        return $this->settings;
    }

    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->create;
    }

    /**
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->update;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->delete;
    }
}
