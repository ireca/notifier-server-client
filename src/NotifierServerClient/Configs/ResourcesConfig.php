<?php
namespace NotifierServerClient\Configs;

class ResourcesConfig extends AbstractBaseConfig
{
    /**
     * @var string
     */
    private $resource = '';

    /**
     * @var string
     */
    private $resources = '';

    /**
     * @var string
     */
    private $create = '';

    /**
     * @var string
     */
    private $update = '';

    /**
     * @var string
     */
    private $delete = '';

    /**
     * @param string $resource
     * @param string $resources
     * @param string $create
     * @param string $update
     * @param string $delete
     *
     */
    public function __construct($resource, $resources, $create, $update, $delete)
    {
        $this->resource = $resource;
        $this->resources = $resources;
        $this->create = $create;
        $this->update = $update;
        $this->delete = $delete;

    }

    /**
     * @return string
     */
    public function getResourceUrl()
    {
        return $this->resource;
    }

    /**
     * @return string
     */
    public function getResourcesUrl()
    {
        return $this->resources;
    }

    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->create;
    }

    /**
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->update;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->delete;
    }
}
