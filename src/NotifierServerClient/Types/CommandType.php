<?php
namespace NotifierServerClient\Types;

class CommandType
{
    const CREATE = 'post';
    const UPDATE = 'put';
    const DELETE = 'delete';
}