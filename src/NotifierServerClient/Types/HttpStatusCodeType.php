<?php
namespace NotifierServerClient\Types;

class HttpStatusCodeType
{
    const OK = 200;
    const CREATED = 201;
    const ACCEPTED = 202;
    const NO_CONTENT = 204;
}
