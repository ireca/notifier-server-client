<?php
namespace NotifierServerClient\Types;

class PriorityType
{
    const HIGHT = 'high';
    const MIDDLE = 'normal';
    const LOW = 'low';
}