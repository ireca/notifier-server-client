<?php
namespace NotifierServerClient\Factories\Services;

use Doctrine\ORM\ORMException;
use NotifierServerClient\Components\Configuration;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Services\Api\CallbackService;
use NotifierServerClient\Services\Api\Http\DefaultSettingsApiService;
use NotifierServerClient\Services\Api\Http\MessagesApiService;
use NotifierServerClient\Services\Api\Http\ResourcesApiService;
use NotifierServerClient\Services\Api\Http\UserApiService;
use NotifierServerClient\Services\Api\Interfaces\DefaultSettingsApiServiceInterface;
use NotifierServerClient\Services\Api\Interfaces\MessagesApiServiceInterface;
use NotifierServerClient\Services\Api\Interfaces\ResourcesApiServiceInterface;
use NotifierServerClient\Services\Api\Interfaces\UserApiServiceInterface;
use NotifierServerClient\Services\ConfigService;
use NotifierServerClient\Services\DefaultSettingsService;
use NotifierServerClient\Services\Factories\HttpCallbackServiceFactory;
use NotifierServerClient\Services\Factories\HttpRequestServiceFactory;
use NotifierServerClient\Services\MessagesService;
use NotifierServerClient\Services\NotifierClientService;
use NotifierServerClient\Services\QueueService;
use NotifierServerClient\Services\RequestServiceInterface;
use NotifierServerClient\Services\ResourcesService;
use NotifierServerClient\Services\UserService;
use NotifierServerClient\Types\ProtocolType;
use RestLog\Factory\MessageLogFactory;
use RestLog\Service\Transport\MessageLogService;
use SlackErrorNotifier\Factory\ErrorHandlerFactory;
use SlackErrorNotifier\Service\ErrorHandlerService;

class NotifierClientFactory
{

    /**
     * @param array $config
     * @param \HumusAmqpModule\Producer $producer
     * @param int $companyId
     * @param int $sourceId
     *
     * @throws ORMException|NotifierClientException
     */
    public static function create($config, $producer = null, $companyId = null, $sourceId = null)
    {
        $configService = self::getConfigService($config);

        $callbackService = HttpCallbackServiceFactory::create($configService);
        $requestService = self::getRequestService($configService);
        $logger = MessageLogFactory::createByConfig($configService->getRestLogConfig())->init($companyId, null, $sourceId);
        $notifier = ErrorHandlerFactory::createByConfig($configService->getSlackNotifier());

        $defaultSettingsApiService = self::getDefaultSettingsApiService($configService, $requestService, $logger, $notifier);
        $messagesApiService = self::getMessagesApiService($configService, $requestService, $logger, $notifier);
        $resourcesApiService = self::getResourcesApiService($configService, $requestService, $logger, $notifier);
        $userApiService = self::getUserApiService($configService, $requestService, $logger, $notifier);

        return new NotifierClientService(
            new CallbackService($callbackService),
            new DefaultSettingsService($defaultSettingsApiService),
            new MessagesService($messagesApiService),
            new ResourcesService($resourcesApiService),
            new UserService($userApiService),
            ! is_null($producer) ? new QueueService($producer, $configService->getQueueConfig()) : null
        );
    }

    /**
     * @param array $config
     *
     * @return ConfigService
     *
     * @throws NotifierClientException
     */
    private static function getConfigService(array $config)
    {
        $configService = new ConfigService(new Configuration(), $config);
        if (! $configService->isValidate()) {
            throw new NotifierClientException($configService->getErrorMessage());
        }

        return $configService;
    }

    /**
     * @param ConfigService $configService
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     *
     * @return DefaultSettingsApiServiceInterface
     *
     * @throws NotifierClientException
     */
    private static function getDefaultSettingsApiService($configService, $requestService, $logger, $notifier)
    {
        switch ($configService->getRequestClient()) {
            case ProtocolType::HTTP_REQUEST_SERVICE :
                $defaultSettingsApiService = new DefaultSettingsApiService($configService->getDefaultSettingsConfig(), $requestService, $logger, $notifier);
                break;
            default:
                throw new NotifierClientException('Unrecognized request client, check config notifierServerClient.requestClient');
        }

        return $defaultSettingsApiService;
    }

    /**
     * @param ConfigService $configService
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     *
     * @return MessagesApiServiceInterface
     *
     * @throws NotifierClientException
     */
    private static function getMessagesApiService($configService, $requestService, $logger, $notifier)
    {
        switch ($configService->getRequestClient()) {
            case ProtocolType::HTTP_REQUEST_SERVICE :
                $messagesApiService = new MessagesApiService($configService->getMessagesConfig(), $requestService, $logger, $notifier);
                break;
            default:
                throw new NotifierClientException('Unrecognized request client, check config notifierServerClient.requestClient');
        }

        return $messagesApiService;
    }

    /**
     * @param ConfigService $configService
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     *
     * @return ResourcesApiServiceInterface
     *
     * @throws NotifierClientException
     */
    private static function getResourcesApiService($configService, $requestService, $logger, $notifier)
    {
        switch ($configService->getRequestClient()) {
            case ProtocolType::HTTP_REQUEST_SERVICE :
                $resourcesApiService = new ResourcesApiService($configService->getResourcesConfig(), $requestService, $logger, $notifier);
                break;
            default:
                throw new NotifierClientException('Unrecognized request client, check config notifierServerClient.requestClient');
        }

        return $resourcesApiService;
    }

    /**
     * @param ConfigService $configService
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     *
     * @return UserApiServiceInterface
     *
     * @throws NotifierClientException
     */
    private static function getUserApiService($configService, $requestService, $logger, $notifier)
    {
        switch ($configService->getRequestClient()) {
            case ProtocolType::HTTP_REQUEST_SERVICE :
                $userApiService = new UserApiService($configService->getUserConfig(), $requestService, $logger, $notifier);
                break;
            default:
                throw new NotifierClientException('Unrecognized request client, check config notifierServerClient.requestClient');
        }

        return $userApiService;
    }

    /**
     * @param ConfigService $configService
     *
     * @return RequestServiceInterface
     *
     * @throws NotifierClientException
     */
    private static function getRequestService($configService)
    {
        switch ($configService->getRequestClient()) {
            case ProtocolType::HTTP_REQUEST_SERVICE :
                $requestService = HttpRequestServiceFactory::create($configService);
                break;
            default:
                throw new NotifierClientException('Unrecognized request client, check config notifierServerClient.requestClient');
        }

        return $requestService;
    }
}
