<?php
namespace NotifierServerClient\Services;

use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Responses\NotifierServerResponse;

interface RequestServiceInterface
{
    /**
     * @param string $url
     * @param string $method
     * @param array $request
     *
     * @return NotifierServerResponse
     *
     * @throws NotifierClientException
     */
    public function send($url, $method, array $request = array());
}
