<?php
namespace NotifierServerClient\Services\Interfaces;

use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Requests\Messages\MessagesCreateExtendedListRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateSimpleListRequest;
use NotifierServerClient\Requests\Messages\MessagesInstantlyCreateListRequest;
use NotifierServerClient\Responses\Messages\MessagesExtendedResponse;
use NotifierServerClient\Responses\Messages\MessagesListResponse;
use NotifierServerClient\Responses\NoContentResponse;

interface MessageInterface
{
    /**
     * @param string $messageId
     *
     * @return MessagesExtendedResponse
     *
     * @throws NotifierClientException
     * @throws Exception
     */
    public function getMessage($messageId);
    /**
     * @param string $request
     *
     * @return MessagesListResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function create($request);

    /**
     * @param MessagesInstantlyCreateListRequest $request
     *
     * @return MessagesListResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function instantlyCreate($request);    

    /**
     * @param string $messageId
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function delete($messageId);

}