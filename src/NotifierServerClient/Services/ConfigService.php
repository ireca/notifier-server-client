<?php
namespace NotifierServerClient\Services;

use Exception;
use NotifierServerClient\Components\Configuration;
use NotifierServerClient\Configs\DefaultSettingsConfig;
use NotifierServerClient\Configs\MessagesConfig;
use NotifierServerClient\Configs\QueueConfig;
use NotifierServerClient\Configs\ResourcesConfig;
use NotifierServerClient\Configs\UserConfig;
use Symfony\Component\Config\Definition\Processor;

class ConfigService
{

    /**
     * @var array
     */
    private $config;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var string
     */
    private $errorMessage = '';

    /**
     * @var string
     */
    private $requestClient = '';

    /**
     * @var array
     */
    private $callbackClientOptions = array();

    /**
     * @var array
     */
    private $httpClientOptions = array();

    /**
     * @var DefaultSettingsConfig
     */
    private $defaultSettingsConfig;

    /**
     * @var MessagesConfig
     */
    private $messagesConfig;

    /**
     * @var QueueConfig
     */
    private $queueConfig;

    /**
     * @var ResourcesConfig
     */
    private $resourcesConfig;

    /**
     * @var UserConfig
     */
    private $userConfig;

    /**
     * @var array
     */
    private $restLogConfig = array();

    /**
     * @var array
     */
    private $slackNotifier = array();

    /**
     * ConfigService constructor.
     *
     * @param Configuration $configuration
     * @param array $config
     */
    public function __construct(Configuration $configuration, $config)
    {
        $this->configuration = $configuration;
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function isValidate()
    {
        $processor = new Processor();
        try {
            $this->setConfig($processor->processConfiguration($this->configuration, $this->config));
        } catch (Exception $ex) {
            $this->setErrorMessage($ex->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @return string
     */
    public function getRequestClient()
    {
        return $this->requestClient;
    }

    /**
     * @return array
     */
    public function getCallbackClientOptions()
    {
        return $this->callbackClientOptions;
    }

    /**
     * @return array
     */
    public function getHttpClientOptions()
    {
        return $this->httpClientOptions;
    }

    /**
     * @return DefaultSettingsConfig
     */
    public function getDefaultSettingsConfig()
    {
        return $this->defaultSettingsConfig;
    }

    /**
     * @return MessagesConfig
     */
    public function getMessagesConfig()
    {
        return $this->messagesConfig;
    }

    /**
     * @return QueueConfig
     */
    public function getQueueConfig()
    {
        return $this->queueConfig;
    }

    /**
     * @return ResourcesConfig
     */
    public function getResourcesConfig()
    {
        return $this->resourcesConfig;
    }

    /**
     * @return UserConfig
     */
    public function getUserConfig()
    {
        return $this->userConfig;
    }

    /**
     * @return array
     */
    public function getRestLogConfig()
    {
        return $this->restLogConfig;
    }

    /**
     * @return array
     */
    public function getSlackNotifier()
    {
        return $this->slackNotifier;
    }

    /**
     * @param array $config
     */
    protected function setConfig($config)
    {
        $this->setRequestClient($config['requestClient']);
        $this->setDefaultSettingsConfig($config['apiUrls']['defaultSettings']);
        $this->setMessagesConfig($config['apiUrls']['messages']);
        $this->setResourcesConfig($config['apiUrls']['resources']);
        $this->setUserConfig($config['apiUrls']['users']);
        $this->setCallbackClientOptions($config['callbackClientOptions']);
        $this->setHttpClientOptions($config['httpClientOptions']);
        $this->setQueueConfig($config['queueOptions']);
        $this->setRestLogConfig($config['restLogConfig']);
        $this->setSlackNotifier($config['slackNotifier']);
    }

    /**
     * @param string $errorMessage
     */
    private function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @param string $requestClient
     */
    private function setRequestClient($requestClient)
    {
        $this->requestClient = $requestClient;
    }

    /**
     * @param array $config
     */
    private function setDefaultSettingsConfig(array $config)
    {
        $this->defaultSettingsConfig = new DefaultSettingsConfig($config['setting'], $config['settings'], $config['create'], $config['update'], $config['delete']);
    }

    /**
     * @param array $config
     */
    private function setMessagesConfig(array $config)
    {
        $this->messagesConfig = new MessagesConfig(
            $config['message'],
            $config['create'],
            $config['instantly_create'],
            $config['delete']
        );
    }

    /**
     * @param QueueConfig $queueConfig
     */
    public function setQueueConfig(array $config)
    {
        $this->queueConfig = new QueueConfig($config['routingKey'], $config['headers']);
    }

    /**
     * @param array $config
     */
    private function setResourcesConfig(array $config)
    {
        $this->resourcesConfig = new ResourcesConfig($config['resource'], $config['resources'], $config['create'], $config['update'], $config['delete']);
    }

    /**
     * @param array $config
     */
    private function setUserConfig(array $config)
    {
        $this->userConfig = new UserConfig($config['user'], $config['create'], $config['update'], $config['delete'], $config['users']);
    }

    /**
     * @param array $config
     */
    private function setCallbackClientOptions(array $config)
    {
        $this->callbackClientOptions = $config;
    }

    /**
     * @param array $config
     */
    private function setHttpClientOptions(array $config)
    {
        $this->httpClientOptions = $config;
    }

    /**
     * @param array $config
     */
    private function setRestLogConfig(array $config)
    {
        $this->restLogConfig = $config;
    }

    /**
     * @param array $config
     */
    private function setSlackNotifier(array $config)
    {
        $this->slackNotifier = $config;
    }
}
