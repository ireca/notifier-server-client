<?php
namespace NotifierServerClient\Services;

use HumusAmqpModule\MessageAttributes;
use NotifierServerClient\Configs\QueueConfig;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Responses\Messages\MessagesExtendedResponse;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Services\Interfaces\MessageInterface;

class QueueService implements MessageInterface
{
    /**
     * @var \HumusAmqpModule\Producer|null
     */
    private $producer = null;

    /**
     * @var QueueConfig
     */
    private $configService = null;

    /**
     * @var string
     */
    private $errorMessage = '';

    /**
     * @param \HumusAmqpModule\Producer|null $producer
     */
    public function __construct($producer, QueueConfig $configService)
    {
        $this->producer = $producer;
        $this->configService = $configService;
    }

    /**
     * @param string $messageId
     *
     * @return MessagesExtendedResponse
     *
     * @throws NotifierClientException
     * @throws Exception
     */
    public function getMessage($messageId)
    {
        throw NotifierClientException("This methot doesn't have realisation!");
    }

    /**
     * @param string $content
     * @return bool
     */
    public function create($content)
    {
        $isSent = false;
        try {
            $messAtributes = new MessageAttributes();
            $messAtributes->setHeaders($this->getConfigService()->getHeaders());
            $this
                ->getProducer()
                ->publish(
                    $content,
                    $this->getConfigService()->getRoutingKey(),
                    $messAtributes
                );
            $isSent = true;
        } catch (\AMQPException $ex) {
            $this->errorMessage = "I can't publish message: " . $ex->getMessage();
        }

        return $isSent;
    }

    /**
     * @param string $messageId
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function delete($messageId)
    {
        throw NotifierClientException("This methot doesn't have realisation!");
    }

    /**
     * @return \HumusAmqpModule\PluginManager\Producer|null
     */
    public function getProducer()
    {
        return $this->producer;
    }

    /**
     * @return ConfigService
     */
    public function getConfigService()
    {
        return $this->configService;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

}