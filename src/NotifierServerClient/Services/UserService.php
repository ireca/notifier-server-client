<?php
namespace NotifierServerClient\Services;

use Exception;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Requests\Users\UserCreateRequest;
use NotifierServerClient\Requests\Users\UserDeleteRequest;
use NotifierServerClient\Requests\Users\UserGetRequest;
use NotifierServerClient\Requests\Users\UsersGetRequest;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Responses\Users\UsersCreateResponse;
use NotifierServerClient\Responses\Users\UsersGetListResponse;
use NotifierServerClient\Responses\Users\UsersGetResponse;
use NotifierServerClient\Services\Api\Interfaces\UserApiServiceInterface;

class UserService
{
    /**
     * @var UserApiServiceInterface
     */
    private $apiService;

    /**
     * @param UserApiServiceInterface $apiService
     */
    public function __construct(UserApiServiceInterface $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param string $userId
     *
     * @return UsersGetResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function getUser($userId)
    {
        $request = new UserGetRequest($userId);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->getUser($request);
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return UsersGetListResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function getUsers($limit, $offset)
    {
        $request = new UsersGetRequest($limit, $offset);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->getUsers($request);
    }

    /**
     * @param UserCreateRequest $request
     *
     * @return UsersCreateResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function create(UserCreateRequest $request)
    {
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->create($request);
    }

    /**
     * @param UserCreateRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function update(UserCreateRequest $request)
    {
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->update($request);
    }

    /**
     * @param string $userId
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function delete($userId)
    {
        $request = new UserDeleteRequest($userId);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->delete($request);
    }

    /**
     * @return UserApiServiceInterface
     */
    private function getApiService()
    {
        return $this->apiService;
    }
}
