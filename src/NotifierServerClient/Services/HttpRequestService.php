<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
namespace NotifierServerClient\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Responses\Factories\HttpResponseFactory;
use NotifierServerClient\Responses\NotifierServerResponse;

class HttpRequestService implements RequestServiceInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $request
     *
     * @return NotifierServerResponse
     *
     * @throws NotifierClientException
     */
    public function send($url, $method, array $request = array())
    {
        try {
            $response = $this->getClient()->request($method, $url, $request);
        } catch (GuzzleException $ex) {
            throw new NotifierClientException($ex->getMessage(), $ex->getCode());
        }

        return HttpResponseFactory::create($response);
    }

    /**
     * @return Client
     */
    private function getClient()
    {
        return $this->client;
    }
}
