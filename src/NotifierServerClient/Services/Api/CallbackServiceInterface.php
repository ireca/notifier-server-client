<?php
namespace NotifierServerClient\Services\Api;

use NotifierServerClient\Responses\Callback\CallbackListResponse;

interface CallbackServiceInterface
{
    /**
     * @param string $url
     *
     * @return CallbackListResponse
     */
    public function send($url);
}
