<?php
namespace NotifierServerClient\Services\Api\Interfaces;

use NotifierServerClient\Requests\Users\UserCreateRequest;
use NotifierServerClient\Requests\Users\UserDeleteRequest;
use NotifierServerClient\Requests\Users\UserGetRequest;
use NotifierServerClient\Requests\Users\UsersGetRequest;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Responses\Users\UsersCreateResponse;
use NotifierServerClient\Responses\Users\UsersGetListResponse;
use NotifierServerClient\Responses\Users\UsersGetResponse;

interface UserApiServiceInterface
{
    /**
     * @param UserGetRequest $request
     *
     * @return UsersGetResponse
     */
    public function getUser(UserGetRequest $request);

    /**
     * @param UsersGetRequest $request
     *
     * @return UsersGetListResponse
     */
    public function getUsers(UsersGetRequest $request);

    /**
     * @param UserCreateRequest $request
     *
     * @return UsersCreateResponse
     */
    public function create(UserCreateRequest $request);

    /**
     * @param UserCreateRequest $request
     *
     * @return NoContentResponse
     */
    public function update(UserCreateRequest $request);

    /**
     * @param UserDeleteRequest $request
     *
     * @return NoContentResponse
     */
    public function delete(UserDeleteRequest $request);
}
