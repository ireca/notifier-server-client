<?php
namespace NotifierServerClient\Services\Api\Interfaces;

use NotifierServerClient\Requests\Resources\ResourceGetRequest;
use NotifierServerClient\Requests\Resources\ResourcesCreateRequest;
use NotifierServerClient\Requests\Resources\ResourcesDeleteRequest;
use NotifierServerClient\Requests\Resources\ResourcesGetRequest;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Responses\Resources\ResourcesCreateResponse;
use NotifierServerClient\Responses\Resources\ResourcesGetListResponse;
use NotifierServerClient\Responses\Resources\ResourcesGetResponse;

interface ResourcesApiServiceInterface
{
    /**
     * @param ResourceGetRequest $request
     *
     * @return ResourcesGetResponse
     */
    public function getResource(ResourceGetRequest $request);

    /**
     * @param ResourcesGetRequest $request
     *
     * @return ResourcesGetListResponse
     */
    public function getResources(ResourcesGetRequest $request);

    /**
     * @param ResourcesCreateRequest $request
     *
     * @return ResourcesCreateResponse
     */
    public function create(ResourcesCreateRequest $request);

    /**
     * @param ResourcesCreateRequest $request
     *
     * @return NoContentResponse
     */
    public function update(ResourcesCreateRequest $request);

    /**
     * @param ResourcesDeleteRequest $request
     *
     * @return NoContentResponse
     */
    public function delete(ResourcesDeleteRequest $request);
}
