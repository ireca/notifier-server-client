<?php
namespace NotifierServerClient\Services\Api\Interfaces;

use NotifierServerClient\Requests\Messages\MessageGetRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateExtendedListRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateSimpleListRequest;
use NotifierServerClient\Requests\Messages\MessagesDeleteRequest;
use NotifierServerClient\Requests\Messages\MessagesInstantlyCreateListRequest;
use NotifierServerClient\Responses\Messages\MessagesExtendedResponse;
use NotifierServerClient\Responses\Messages\MessagesListResponse;
use NotifierServerClient\Responses\NoContentResponse;

interface MessagesApiServiceInterface
{
    /**
     * @param MessageGetRequest $request
     *
     * @return MessagesExtendedResponse
     */
    public function getMessage(MessageGetRequest $request);

    /**
     * @param MessagesCreateExtendedListRequest|MessagesCreateSimpleListRequest $request
     *
     * @return MessagesListResponse
     */
    public function create($request);

    /**
     * @param MessagesInstantlyCreateListRequest $request
     *
     * @return MessagesListResponse
     */
    public function instantlyCreate(MessagesInstantlyCreateListRequest $request);

    /**
     * @param MessagesDeleteRequest $request
     *
     * @return NoContentResponse
     */
    public function delete(MessagesDeleteRequest $request);
}
