<?php
namespace NotifierServerClient\Services\Api\Interfaces;

use NotifierServerClient\Requests\DefaultSettings\DefaultSettingGetRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsCreateRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsDeleteRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsGetRequest;
use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsListResponse;
use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsResponse;
use NotifierServerClient\Responses\NoContentResponse;

interface DefaultSettingsApiServiceInterface
{
    /**
     * @param DefaultSettingGetRequest $request
     *
     * @return DefaultSettingsResponse
     */
    public function getSetting(DefaultSettingGetRequest $request);

    /**
     * @param DefaultSettingsGetRequest $request
     *
     * @return DefaultSettingsListResponse
     */
    public function getSettings(DefaultSettingsGetRequest $request);

    /**
     * @param DefaultSettingsCreateRequest $request
     *
     * @return NoContentResponse
     */
    public function create(DefaultSettingsCreateRequest $request);

    /**
     * @param DefaultSettingsCreateRequest $request
     *
     * @return NoContentResponse
     */
    public function update(DefaultSettingsCreateRequest $request);

    /**
     * @param DefaultSettingsDeleteRequest $request
     *
     * @return NoContentResponse
     */
    public function delete(DefaultSettingsDeleteRequest $request);
}
