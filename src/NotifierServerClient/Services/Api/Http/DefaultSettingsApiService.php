<?php
namespace NotifierServerClient\Services\Api\Http;

use NotifierServerClient\Configs\DefaultSettingsConfig;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingGetRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsCreateRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsDeleteRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsGetRequest;
use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsListResponse;
use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsResponse;
use NotifierServerClient\Responses\Factories\DefaultSettings\DefaultSettingsListResponseFactory;
use NotifierServerClient\Responses\Factories\DefaultSettings\DefaultSettingsResponseFactory;
use NotifierServerClient\Responses\Factories\NoContentResponseFactory;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Services\Api\Interfaces\DefaultSettingsApiServiceInterface;
use NotifierServerClient\Services\RequestServiceInterface;
use NotifierServerClient\Types\HttpStatusCodeType;
use RestLog\Service\Transport\MessageLogService;
use SlackErrorNotifier\Service\ErrorHandlerService;

class DefaultSettingsApiService extends AbstractBaseLicenseServer implements DefaultSettingsApiServiceInterface
{
    /**
     * @var DefaultSettingsConfig
     */
    private $defaultSettingsConfig;

    /**
     * @param DefaultSettingsConfig $defaultSettingsConfig
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     */
    public function __construct(DefaultSettingsConfig $defaultSettingsConfig, RequestServiceInterface $requestService, MessageLogService $logger, ErrorHandlerService $notifier)
    {
        parent::__construct($requestService, $logger, $notifier);
        $this->defaultSettingsConfig = $defaultSettingsConfig;
    }

    /**
     * @param DefaultSettingGetRequest $request
     *
     * @return DefaultSettingsResponse
     *
     * @throws NotifierClientException
     */
    public function getSetting(DefaultSettingGetRequest $request)
    {
        $response = $this->sendGetRequest($request->toArray(), $this->getDefaultSettingsConfig()->getSettingUrl());
        if ($response->code !== HttpStatusCodeType::OK) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return DefaultSettingsResponseFactory::create($content);
    }

    /**
     * @param DefaultSettingsGetRequest $request
     *
     * @return DefaultSettingsListResponse
     *
     * @throws NotifierClientException
     */
    public function getSettings(DefaultSettingsGetRequest $request)
    {
        $response = $this->sendGetRequest($request->toArray(), $this->getDefaultSettingsConfig()->getSettingsUrl());
        if ($response->code !== HttpStatusCodeType::OK) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return DefaultSettingsListResponseFactory::create($content);
    }

    /**
     * @param DefaultSettingsCreateRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException
     */
    public function create(DefaultSettingsCreateRequest $request)
    {
        $response = $this->sendPostRequest($request->toArray(), $this->getDefaultSettingsConfig()->getCreateUrl());
        if ($response->code !== HttpStatusCodeType::CREATED) {
            throw new NotifierClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @param DefaultSettingsCreateRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException
     */
    public function update(DefaultSettingsCreateRequest $request)
    {
        $response = $this->sendPutRequest($request->toArray(), $this->getDefaultSettingsConfig()->getUpdateUrl());
        if ($response->code !== HttpStatusCodeType::NO_CONTENT) {
            throw new NotifierClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @param DefaultSettingsDeleteRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException
     */
    public function delete(DefaultSettingsDeleteRequest $request)
    {
        $response = $this->sendDeleteRequest($request->toArray(), $this->getDefaultSettingsConfig()->getDeleteUrl());
        if ($response->code !== HttpStatusCodeType::ACCEPTED) {
            throw new NotifierClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @return DefaultSettingsConfig
     */
    private function getDefaultSettingsConfig()
    {
        return $this->defaultSettingsConfig;
    }
}
