<?php
namespace NotifierServerClient\Services\Api\Http;

use Exception;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Responses\NotifierServerResponse;
use NotifierServerClient\Services\RequestServiceInterface;
use RestLog\Service\Transport\MessageLogService;
use SlackErrorNotifier\Service\ErrorHandlerService;

abstract class AbstractBaseLicenseServer
{
    /**
     * @var RequestServiceInterface
     */
    private $requestService;

    /**
     * HttpRequestService to save any errors
     * @var MessageLogService
     */
    private $logger;

    /**
     * HttpRequestService to send any errors
     * @var ErrorHandlerService
     */
    private $notifier;

    /**
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     */
    public function __construct(
        RequestServiceInterface $requestService,
        MessageLogService          $logger,
        ErrorHandlerService        $notifier
    )
    {
        $this->requestService = $requestService;
        $this->logger = $logger;
        $this->notifier = $notifier;
    }

    /**
     * @param array $request
     * @param string $url
     *
     * @return NotifierServerResponse
     */
    protected function sendGetRequest(array $request, $url)
    {
        $request = array('query' => $request);
        return $this->send($url, 'get', $request);
    }

    /**
     * @param array $request
     * @param string $url
     *
     * @return NotifierServerResponse
     */
    protected function sendPostRequest(array $request, $url)
    {
        $request = array('body' => json_encode($request));
        return $this->send($url, 'post', $request);
    }

    /**
     * @param array $request
     * @param string $url
     *
     * @return NotifierServerResponse
     */
    protected function sendPutRequest(array $request, $url)
    {
        $request = array('body' => json_encode($request));
        return $this->send($url, 'put', $request);
    }

    /**
     * @param array $request
     * @param string $url
     *
     * @return NotifierServerResponse
     */
    protected function sendDeleteRequest(array $request, $url)
    {
        $request = array('body' => json_encode($request));
        return $this->send($url, 'delete', $request);
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $request
     *
     * @return NotifierServerResponse
     */
    private function send($url, $method, array $request)
    {
        try {
            $response = $this
                ->getRequestService()
                ->send($url, $method, $request);

        } catch (NotifierClientException $ex) {
            $this->saveLog($ex->getMessage());
            $this->sendNotify($ex->getMessage());

            $response = new NotifierServerResponse($ex->getCode(), $ex->getMessage());
        }

        return $response;
    }

    /**
     * @return RequestServiceInterface
     */
    private function getRequestService()
    {
        return $this->requestService;
    }

    /**
     * @return MessageLogService|null
     */
    private function getLogger()
    {
        return $this->logger;
    }

    /**
     * @return ErrorHandlerService|null
     */
    private function getNotifier()
    {
        return $this->notifier;
    }


    /**
     * Save log to DB
     * @param string $message
     *
     * @return bool
     */
    protected function saveLog($message)
    {
        if (! is_null($this->getLogger())
            && $this->getLogger()->isActive() === true) {
            try {
                $this->getLogger()->critical($message);
            } catch (Exception $ex) {
                //Not handling errors of the logging because we have notifier.
            }

            return true;
        }

        return false;
    }

    /**
     * Send log to slack channel
     * @param string $message
     * @param integer|null $logId
     *
     * @return boolean
     *
     * @throws ErrorHandlerService
     */
    protected function sendNotify($message, $logId = null)
    {
        if (! is_null($this->getNotifier())) {
            return $this
                ->getNotifier()
                ->getNotifierService()
                ->sendLogNotify($logId, $message);
        }

        return false;
    }
}
