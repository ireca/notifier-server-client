<?php
namespace NotifierServerClient\Services\Api\Http;

use NotifierServerClient\Configs\ResourcesConfig;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Requests\Resources\ResourceGetRequest;
use NotifierServerClient\Requests\Resources\ResourcesCreateRequest;
use NotifierServerClient\Requests\Resources\ResourcesDeleteRequest;
use NotifierServerClient\Requests\Resources\ResourcesGetRequest;
use NotifierServerClient\Responses\Factories\NoContentResponseFactory;
use NotifierServerClient\Responses\Factories\Resources\ResourceCreateResponseFactory;
use NotifierServerClient\Responses\Factories\Resources\ResourcesGetListResponseFactory;
use NotifierServerClient\Responses\Factories\Resources\ResourcesGetResponseFactory;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Responses\Resources\ResourcesCreateResponse;
use NotifierServerClient\Responses\Resources\ResourcesGetListResponse;
use NotifierServerClient\Responses\Resources\ResourcesGetResponse;
use NotifierServerClient\Services\Api\Interfaces\ResourcesApiServiceInterface;
use NotifierServerClient\Services\RequestServiceInterface;
use NotifierServerClient\Types\HttpStatusCodeType;
use RestLog\Service\Transport\MessageLogService;
use SlackErrorNotifier\Service\ErrorHandlerService;

class ResourcesApiService extends AbstractBaseLicenseServer implements ResourcesApiServiceInterface
{
    /**
     * @var ResourcesConfig
     */
    private $resourcesConfig;

    /**
     * @param ResourcesConfig $resourcesConfig
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     */
    public function __construct(ResourcesConfig $resourcesConfig, RequestServiceInterface $requestService, MessageLogService $logger, ErrorHandlerService $notifier)
    {
        parent::__construct($requestService, $logger, $notifier);
        $this->resourcesConfig = $resourcesConfig;
    }

    /**
     * @param ResourceGetRequest $request
     *
     * @return ResourcesGetResponse
     *
     * @throws NotifierClientException
     */
    public function getResource(ResourceGetRequest $request)
    {
        $response = $this->sendGetRequest($request->toArray(), $this->getResourcesConfig()->getResourceUrl());
        if ($response->code !== HttpStatusCodeType::OK) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return ResourcesGetResponseFactory::create($content);
    }

    /**
     * @param ResourcesGetRequest $request
     *
     * @return ResourcesGetListResponse
     *
     * @throws NotifierClientException
     */
    public function getResources(ResourcesGetRequest $request)
    {
        $response = $this->sendGetRequest($request->toArray(), $this->getResourcesConfig()->getResourcesUrl());
        if ($response->code !== HttpStatusCodeType::OK) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return ResourcesGetListResponseFactory::create($content);
    }

    /**
     * @param ResourcesCreateRequest $request
     *
     * @return ResourcesCreateResponse
     *
     * @throws NotifierClientException
     */
    public function create(ResourcesCreateRequest $request)
    {
        $response = $this->sendPostRequest($request->toArray(), $this->getResourcesConfig()->getCreateUrl());
        if ($response->code !== HttpStatusCodeType::CREATED) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return ResourceCreateResponseFactory::create($content);
    }

    /**
     * @param ResourcesCreateRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException
     */
    public function update(ResourcesCreateRequest $request)
    {
        $response = $this->sendPutRequest($request->toArray(), $this->getResourcesConfig()->getUpdateUrl());
        if ($response->code !== HttpStatusCodeType::NO_CONTENT) {
            throw new NotifierClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @param ResourcesDeleteRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException
     */
    public function delete(ResourcesDeleteRequest $request)
    {
        $response = $this->sendDeleteRequest($request->toArray(), $this->getResourcesConfig()->getDeleteUrl());
        if ($response->code !== HttpStatusCodeType::ACCEPTED) {
            throw new NotifierClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @return ResourcesConfig
     */
    private function getResourcesConfig()
    {
        return $this->resourcesConfig;
    }
}
