<?php
namespace NotifierServerClient\Services\Api\Http;

use NotifierServerClient\Configs\MessagesConfig;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Requests\Messages\MessageGetRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateExtendedListRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateSimpleListRequest;
use NotifierServerClient\Requests\Messages\MessagesDeleteRequest;
use NotifierServerClient\Requests\Messages\MessagesInstantlyCreateListRequest;
use NotifierServerClient\Responses\Factories\Messages\MessagesExtendedResponseFactory;
use NotifierServerClient\Responses\Factories\Messages\MessagesListResponseFactory;
use NotifierServerClient\Responses\Factories\NoContentResponseFactory;
use NotifierServerClient\Responses\Messages\MessagesExtendedResponse;
use NotifierServerClient\Responses\Messages\MessagesListResponse;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Services\Api\Interfaces\MessagesApiServiceInterface;
use NotifierServerClient\Services\RequestServiceInterface;
use NotifierServerClient\Types\HttpStatusCodeType;
use RestLog\Service\Transport\MessageLogService;
use SlackErrorNotifier\Service\ErrorHandlerService;

class MessagesApiService extends AbstractBaseLicenseServer implements MessagesApiServiceInterface
{
    /**
     * @var MessagesConfig
     */
    private $messagesConfig;

    /**
     * @param MessagesConfig $messagesConfig
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     */
    public function __construct(MessagesConfig $messagesConfig, RequestServiceInterface $requestService, MessageLogService $logger, ErrorHandlerService $notifier)
    {
        parent::__construct($requestService, $logger, $notifier);
        $this->messagesConfig = $messagesConfig;
    }

    /**
     * @param MessageGetRequest $request
     *
     * @return MessagesExtendedResponse
     *
     * @throws NotifierClientException
     */
    public function getMessage(MessageGetRequest $request)
    {
        $response = $this->sendGetRequest($request->toArray(), $this->getMessagesConfig()->getMessageUrl());
        if ($response->code !== HttpStatusCodeType::OK) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return MessagesExtendedResponseFactory::create($content);
    }

    /**
     * @param MessagesCreateExtendedListRequest|MessagesCreateSimpleListRequest $request
     *
     * @return MessagesListResponse
     *
     * @throws NotifierClientException
     */
    public function create($request)
    {
        $response = $this->sendPostRequest($request->toArray(), $this->getMessagesConfig()->getCreateUrl());
        if ($response->code !== HttpStatusCodeType::CREATED) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return MessagesListResponseFactory::create($content);
    }

    /**
     * @param MessagesInstantlyCreateListRequest $request
     *
     * @return MessagesListResponse
     *
     * @throws NotifierClientException
     */
    public function instantlyCreate(MessagesInstantlyCreateListRequest $request)
    {
        $response = $this->sendPostRequest($request->toArray(), $this->getMessagesConfig()->getInstantlyCreateUrl());
        if ($response->code !== HttpStatusCodeType::CREATED) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return MessagesListResponseFactory::create($content);
    }

    /**
     * @param MessagesDeleteRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException
     */
    public function delete(MessagesDeleteRequest $request)
    {
        $response = $this->sendDeleteRequest($request->toArray(), $this->getMessagesConfig()->getDeleteUrl());
        if ($response->code !== HttpStatusCodeType::ACCEPTED) {
            throw new NotifierClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @return MessagesConfig
     */
    private function getMessagesConfig()
    {
        return $this->messagesConfig;
    }
}
