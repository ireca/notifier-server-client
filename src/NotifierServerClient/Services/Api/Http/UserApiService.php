<?php
namespace NotifierServerClient\Services\Api\Http;

use NotifierServerClient\Configs\UserConfig;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Requests\Users\UserCreateRequest;
use NotifierServerClient\Requests\Users\UserDeleteRequest;
use NotifierServerClient\Requests\Users\UserGetRequest;
use NotifierServerClient\Requests\Users\UsersGetRequest;
use NotifierServerClient\Responses\Factories\NoContentResponseFactory;
use NotifierServerClient\Responses\Factories\Users\UsersCreateResponseFactory;
use NotifierServerClient\Responses\Factories\Users\UsersGetListResponseFactory;
use NotifierServerClient\Responses\Factories\Users\UsersGetResponseFactory;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Responses\Users\UsersCreateResponse;
use NotifierServerClient\Responses\Users\UsersGetListResponse;
use NotifierServerClient\Responses\Users\UsersGetResponse;
use NotifierServerClient\Services\Api\Interfaces\UserApiServiceInterface;
use NotifierServerClient\Services\RequestServiceInterface;
use NotifierServerClient\Types\HttpStatusCodeType;
use RestLog\Service\Transport\MessageLogService;
use SlackErrorNotifier\Service\ErrorHandlerService;

class UserApiService extends AbstractBaseLicenseServer implements UserApiServiceInterface
{
    /**
     * @var UserConfig
     */
    private $userConfig;

    /**
     * @param UserConfig $userConfig
     * @param RequestServiceInterface $requestService
     * @param MessageLogService $logger
     * @param ErrorHandlerService $notifier
     */
    public function __construct(UserConfig $userConfig, RequestServiceInterface $requestService, MessageLogService $logger, ErrorHandlerService $notifier)
    {
        parent::__construct($requestService, $logger, $notifier);
        $this->userConfig = $userConfig;
    }

    /**
     * @param UserGetRequest $request
     *
     * @return UsersGetResponse
     *
     * @throws NotifierClientException
     */
    public function getUser(UserGetRequest $request)
    {
        $response = $this->sendGetRequest($request->toArray(), $this->getUserConfig()->getUserUrl());
        if ($response->code !== HttpStatusCodeType::OK) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return UsersGetResponseFactory::create($content);
    }

    /**
     * @param UsersGetRequest $request
     *
     * @return UsersGetListResponse
     *
     * @throws NotifierClientException
     */
    public function getUsers(UsersGetRequest $request)
    {
        $response = $this->sendGetRequest($request->toArray(), $this->getUserConfig()->getUsersUrl());
        if ($response->code !== HttpStatusCodeType::OK) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return UsersGetListResponseFactory::create($content);
    }

    /**
     * @param UserCreateRequest $request
     *
     * @return UsersCreateResponse
     *
     * @throws NotifierClientException
     */
    public function create(UserCreateRequest $request)
    {
        $response = $this->sendPostRequest($request->toArray(), $this->getUserConfig()->getCreateUrl());
        if ($response->code !== HttpStatusCodeType::CREATED) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return UsersCreateResponseFactory::create($content);
    }

    /**
     * @param UserCreateRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException
     */
    public function update(UserCreateRequest $request)
    {
        $response = $this->sendPutRequest($request->toArray(), $this->getUserConfig()->getUpdateUrl());
        if ($response->code !== HttpStatusCodeType::NO_CONTENT) {
            throw new NotifierClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @param UserDeleteRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException
     */
    public function delete(UserDeleteRequest $request)
    {
        $response = $this->sendDeleteRequest($request->toArray(), $this->getUserConfig()->getDeleteUrl());
        if ($response->code !== HttpStatusCodeType::ACCEPTED) {
            throw new NotifierClientException($response->content, $response->code);
        }

        return NoContentResponseFactory::create();
    }

    /**
     * @return UserConfig
     */
    private function getUserConfig()
    {
        return $this->userConfig;
    }
}
