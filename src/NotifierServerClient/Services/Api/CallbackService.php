<?php
namespace NotifierServerClient\Services\Api;

use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Responses\Callback\CallbackListResponse;
use NotifierServerClient\Responses\Factories\Callback\CallbackListResponseFactory;
use NotifierServerClient\Services\RequestServiceInterface;
use NotifierServerClient\Types\HttpStatusCodeType;

class CallbackService implements CallbackServiceInterface
{
    /**
     * @var RequestServiceInterface
     */
    private $requestService;

    /**
     * @param RequestServiceInterface $requestService
     */
    public function __construct(RequestServiceInterface $requestService)
    {
        $this->requestService = $requestService;
    }

    /**
     * @param string $url
     *
     * @return CallbackListResponse
     *
     * @throws NotifierClientException
     */
    public function send($url)
    {
        $response = $this->getRequestService()->send($url, 'POST');
        if ($response->code !== HttpStatusCodeType::CREATED) {
            throw new NotifierClientException($response->content, $response->code);
        }

        $content = json_decode($response->content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new NotifierClientException('Syntax error, malformed JSON', json_last_error());
        }

        return CallbackListResponseFactory::create($content);
    }

    /**
     * @return RequestServiceInterface
     */
    private function getRequestService()
    {
        return $this->requestService;
    }
}
