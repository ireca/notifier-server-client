<?php
namespace NotifierServerClient\Services;

use Exception;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingGetRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsCreateRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsDeleteRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsGetRequest;
use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsListResponse;
use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsResponse;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Services\Api\Interfaces\DefaultSettingsApiServiceInterface;

class DefaultSettingsService
{
    /**
     * @var DefaultSettingsApiServiceInterface
     */
    private $apiService;

    /**
     * @param DefaultSettingsApiServiceInterface $apiService
     */
    public function __construct(DefaultSettingsApiServiceInterface $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param string $settingId
     *
     * @return DefaultSettingsResponse
     *
     * @throws NotifierClientException
     * @throws Exception
     */
    public function getSetting($settingId)
    {
        $request = new DefaultSettingGetRequest($settingId);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->getSetting($request);
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return DefaultSettingsListResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function getSettings($limit, $offset)
    {
        $request = new DefaultSettingsGetRequest($limit, $offset);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->getSettings($request);
    }

    /**
     * @param DefaultSettingsCreateRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function create(DefaultSettingsCreateRequest $request)
    {
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->create($request);
    }

    /**
     * @param DefaultSettingsCreateRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function update(DefaultSettingsCreateRequest $request)
    {
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->update($request);
    }

    /**
     * @param string $settingId
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function delete($settingId)
    {
        $request = new DefaultSettingsDeleteRequest($settingId);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->delete($request);
    }

    /**
     * @return DefaultSettingsApiServiceInterface
     */
    private function getApiService()
    {
        return $this->apiService;
    }
}
