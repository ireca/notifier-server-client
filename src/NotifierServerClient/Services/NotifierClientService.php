<?php
namespace NotifierServerClient\Services;

use NotifierServerClient\Services\Api\CallbackServiceInterface;

class NotifierClientService
{
    /**
     * @var CallbackServiceInterface
     */
    private $callbackService;

    /**
     * @var DefaultSettingsService
     */
    private $defaultSettingsService;

    /**
     * @var MessagesService
     */
    private $messagesService;

    /**
     * @var ResourcesService
     */
    private $resourcesService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var QueueService|null
     */
    private $queueService;

    /**
     * @param CallbackServiceInterface $callbackService
     * @param DefaultSettingsService $defaultSettingsService
     * @param MessagesService $messagesService
     * @param ResourcesService $resourcesService
     * @param UserService $userService
     */
    function __construct(
        CallbackServiceInterface $callbackService,
        DefaultSettingsService   $defaultSettingsService,
        MessagesService          $messagesService,
        ResourcesService         $resourcesService,
        UserService              $userService,
        $queueService = null
    )
    {
        $this->callbackService = $callbackService;
        $this->defaultSettingsService = $defaultSettingsService;
        $this->messagesService = $messagesService;
        $this->resourcesService = $resourcesService;
        $this->userService = $userService;
        $this->queueService = $queueService;
    }

    /**
     * @return CallbackServiceInterface
     */
    public function getCallbackService()
    {
        return $this->callbackService;
    }

    /**
     * @return DefaultSettingsService
     */
    public function getDefaultSettingsService()
    {
        return $this->defaultSettingsService;
    }

    /**
     * @return MessagesService
     */
    public function getMessagesService()
    {
        return $this->messagesService;
    }

    /**
     * @return ResourcesService
     */
    public function getResourcesService()
    {
        return $this->resourcesService;
    }

    /**
     * @return UserService
     */
    public function getUserService()
    {
        return $this->userService;
    }

    /**
     * @return QueueService|null
     */
    public function getQueueService()
    {
        return $this->queueService;
    }

}
