<?php
namespace NotifierServerClient\Services;

use Exception;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Requests\Messages\MessageGetRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateExtendedListRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateSimpleListRequest;
use NotifierServerClient\Requests\Messages\MessagesDeleteRequest;
use NotifierServerClient\Requests\Messages\MessagesInstanlyCreateRequest;
use NotifierServerClient\Requests\Messages\MessagesInstantlyCreateListRequest;
use NotifierServerClient\Responses\Messages\MessagesExtendedResponse;
use NotifierServerClient\Responses\Messages\MessagesListResponse;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Services\Api\Interfaces\MessagesApiServiceInterface;
use NotifierServerClient\Services\Interfaces\MessageInterface;

class MessagesService implements MessageInterface
{
    /**
     * @var MessagesApiServiceInterface
     */
    private $apiService;

    /**
     * @param MessagesApiServiceInterface $apiService
     */
    public function __construct(MessagesApiServiceInterface $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param string $messageId
     *
     * @return MessagesExtendedResponse
     *
     * @throws NotifierClientException
     * @throws Exception
     */
    public function getMessage($messageId)
    {
        $request = new MessageGetRequest($messageId);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->getMessage($request);
    }

    /**
     * @param MessagesCreateExtendedListRequest|MessagesCreateSimpleListRequest $request
     *
     * @return MessagesListResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function create($request)
    {
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->create($request);
    }

    /**
     * @param MessagesInstantlyCreateListRequest $request
     *
     * @return MessagesListResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function instantlyCreate($request)
    {
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->instantlyCreate($request);
    }

    /**
     * @param string $messageId
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function delete($messageId)
    {
        $request = new MessagesDeleteRequest($messageId);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->delete($request);
    }

    /**
     * @return MessagesApiServiceInterface
     */
    private function getApiService()
    {
        return $this->apiService;
    }
}
