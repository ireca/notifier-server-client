<?php
namespace NotifierServerClient\Services;

use Exception;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Requests\Resources\ResourceGetRequest;
use NotifierServerClient\Requests\Resources\ResourcesCreateRequest;
use NotifierServerClient\Requests\Resources\ResourcesDeleteRequest;
use NotifierServerClient\Requests\Resources\ResourcesGetRequest;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Responses\Resources\ResourcesCreateResponse;
use NotifierServerClient\Responses\Resources\ResourcesGetListResponse;
use NotifierServerClient\Responses\Resources\ResourcesGetResponse;
use NotifierServerClient\Services\Api\Interfaces\ResourcesApiServiceInterface;

class ResourcesService
{
    /**
     * @var ResourcesApiServiceInterface
     */
    private $apiService;

    /**
     * @param ResourcesApiServiceInterface $apiService
     */
    public function __construct(ResourcesApiServiceInterface $apiService)
    {
        $this->apiService = $apiService;

    }

    /**
     * @param string $resourceId
     *
     * @return ResourcesGetResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function getResource($resourceId)
    {
        $request = new ResourceGetRequest($resourceId);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->getResource($request);
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return ResourcesGetListResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function getResources($limit, $offset)
    {
        $request = new ResourcesGetRequest($limit, $offset);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->getResources($request);
    }

    /**
     * @param ResourcesCreateRequest $request
     *
     * @return ResourcesCreateResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function create(ResourcesCreateRequest $request)
    {
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->create($request);
    }

    /**
     * @param ResourcesCreateRequest $request
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function update(ResourcesCreateRequest $request)
    {
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->update($request);
    }

    /**
     * @param string $resourceId
     *
     * @return NoContentResponse
     *
     * @throws NotifierClientException|Exception
     */
    public function delete($resourceId)
    {
        $request = new ResourcesDeleteRequest($resourceId);
        if ($request->validate() === false) {
            throw new NotifierClientException($request->getStringErrors());
        }

        return $this->getApiService()->delete($request);
    }

    /**
     * @return ResourcesApiServiceInterface
     */
    private function getApiService()
    {
        return $this->apiService;
    }
}
