<?php
namespace NotifierServerClient\Services\Factories;

use GuzzleHttp\Client;
use NotifierServerClient\Services\ConfigService;
use NotifierServerClient\Services\HttpRequestService;
use NotifierServerClient\Services\RequestServiceInterface;

class HttpCallbackServiceFactory
{
    /**
     * @param ConfigService $configService
     *
     * @return RequestServiceInterface
     */
    public static function create(ConfigService $configService)
    {
        $config = array(
            'timeout' => $configService->getCallbackClientOptions()['timeout'],
            'headers' => $configService->getCallbackClientOptions()['headers']
        );

        return new HttpRequestService(new Client($config));
    }
}
