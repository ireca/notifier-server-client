<?php
namespace NotifierServerClient\Services\Factories;

use GuzzleHttp\Client;
use NotifierServerClient\Services\ConfigService;
use NotifierServerClient\Services\HttpRequestService;
use NotifierServerClient\Services\RequestServiceInterface;

class HttpRequestServiceFactory
{
    /**
     * @param ConfigService $configService
     *
     * @return RequestServiceInterface
     */
    public static function create(ConfigService $configService)
    {
        $config = array(
            'base_uri' => $configService->getHttpClientOptions()['baseUri'],
            'timeout' => $configService->getHttpClientOptions()['timeout'],
            'headers' => $configService->getHttpClientOptions()['headers']
        );

        return new HttpRequestService(new Client($config));
    }
}
