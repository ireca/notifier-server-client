<?php
namespace NotifierServerClient\Requests;

use Exception;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

abstract class AbstractBaseRequest
{
    /**
     * @var ConstraintViolationList|null
     */
    private $errors = null;

    /**
     * @return Collection
     */
    abstract public function getConstraints();

    /**
     * @return bool
     *
     * @throws Exception
     */
    public function validate()
    {
        $validator = Validation::createValidator();
        $constraintViolationList = $validator->validate($this->toArray(), $this->getConstraints());
        if ($constraintViolationList->count() > 0) {
            $this->addError($constraintViolationList);

            return false;
        }

        return true;
    }

    /**
     * @return ConstraintViolationList|null
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return string
     */
    public function getStringErrors()
    {
        if ($this->errors instanceof ConstraintViolationList) {
            return $this->errors->__toString();
        }

        return '';
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return json_decode(json_encode($this), true);
    }

    /**
     * @param ConstraintViolationListInterface|ConstraintViolationInterface $otherList
     */
    private function addError($otherList)
    {
        if ($this->errors === null) {
            $this->errors = new ConstraintViolationList();
        }

        if ($otherList instanceof ConstraintViolationListInterface) {
            $this->errors->addAll($otherList);
        } elseif ($otherList instanceof ConstraintViolationInterface) {
            $this->errors->add($otherList);
        }
    }
}
