<?php
namespace NotifierServerClient\Requests\Users;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Constraints\Collection;

class UsersGetRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var int
     */
    public $limit;

    /**
     * @var int
     */
    public $offset;

    /**
     * @param int $limit
     * @param int $offset
     */
    public function __construct($limit, $offset)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     * @return Collection
     */
    public function getConstraints()
    {
        return new Collection(array(
            'limit' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('integer')
            ),
            'offset' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('integer')
            ),
        ));
    }
}
