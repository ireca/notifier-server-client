<?php
namespace NotifierServerClient\Requests\Users;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Constraints\Collection;

class UserCreateRequest extends AbstractBaseRequest implements RequestInterface
{

    /**
     * @var string
     */
    public $userId = '';

    /**
     * @var string
     */
    public $role = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @param string $userId
     * @param string $role
     * @param string $title
     * @param string $description
     */
    public function __construct($userId, $role, $title, $description)
    {
        $this->userId = $userId;
        $this->role = $role;
        $this->title = $title;
        $this->description = $description;
    }

    /**
     * @return Collection
     */
    public function getConstraints()
    {
        return new Collection(array(
            'userId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 64)),
            ),
            'role' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Choice(array('choices' => array('device', 'service'))),
            ),
            'title' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 100)),
            ),
            'description' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 256)),
            ),
        ));
    }
}
