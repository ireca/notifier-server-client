<?php
namespace NotifierServerClient\Requests\Messages;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;

class MessagesInstantlyCreateListRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var array
     */
    private $elements = array();

    /**
     * @return Constraints\All
     */
    public function getConstraints()
    {
        return new Constraints\All(
            new Constraints\Collection(array(
                'messageId' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 64)),
                ),
                'resource' => new Constraints\Collection(array(
                    'url' => array(
                        new Constraints\NotNull(),
                        new Constraints\NotBlank(),
                        new Constraints\Type('string'),
                        new Constraints\Length(array('min' => 1, 'max' => 1000)),
                        new Constraints\Url(),
                    ),
                    'description' => array(
                        new Constraints\Optional()
                    ),
                    'setting' => array(
                        new Constraints\Optional()
                    ),
                )),
                'urlParams' => array(
                    new Constraints\Optional()
                ),
                'headers' => array(
                    new Constraints\Optional()
                ),
                'priority' => array(
                    new Constraints\Optional(),
                    new Constraints\Type('string'),
                ),
                'command' => array(
                    new Constraints\Optional(),
                    new Constraints\Type('string'),
                ),
                'content' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 4096)),
                ),
            ))
        );
    }

    /**
     * @param MessagesInstanlyCreateRequest $element
     *
     * @return true
     */
    public function add(MessagesInstanlyCreateRequest $element)
    {
        $this->elements[] = $element;

        return true;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return json_decode(json_encode($this->elements), true);
    }
}
