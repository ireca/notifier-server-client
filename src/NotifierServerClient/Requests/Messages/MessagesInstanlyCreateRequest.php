<?php
namespace NotifierServerClient\Requests\Messages;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use NotifierServerClient\Types\CommandType;
use NotifierServerClient\Types\PriorityType;
use NotifierServerClient\Requests\Resources\ResourcesCreateRequest;
use Symfony\Component\Validator\Constraints;

class MessagesInstanlyCreateRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $messageId = '';

    /**
     * @var ResourcesCreateRequest
     */
    public $resource = null;

    /**
     * @var array
     */
    public $urlParams = array();

    /**
     * @var array
     */
    public $headers = array();

    /**
     * @var string
     */
    public $priority = '';

    /**
     * @var string
     */
    public $command = '';

    /**
     * @var string
     */
    public $content = '';

    /**
     * @param string $messageId
     * @param string $resourceUrl
     * @param array $urlParams
     * @param array $headers
     * @param string $command
     * @param string $content
     */
    public function __construct($messageId, ResourcesCreateRequest $resource, $content = "", $urlParams = array(), $headers = array(), $command = CommandType::CREATE)
    {
        $this->messageId = $messageId;
        $this->resource = $resource;
        $this->urlParams = $urlParams;
        $this->headers = $headers;
        $this->command = $command;
        $this->content = $content;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'fields' => array(
                'messageId' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 64)),
                ),
                'resource' => new Constraints\Collection(array(
                    'url' => array(
                        new Constraints\NotNull(),
                        new Constraints\NotBlank(),
                        new Constraints\Type('string'),
                        new Constraints\Length(array('min' => 1, 'max' => 1000)),
                        new Constraints\Url(),
                    )
                )),
                'urlParams' => array(
                    new Constraints\Optional()
                ),
                'headers' => array(
                    new Constraints\Optional()
                ),
                'priority' => array(
                    new Constraints\Optional(),
                    new Constraints\Type('string'),
                ),
                'command' => array(
                    new Constraints\Optional(),
                    new Constraints\Type('string'),
                ),
                'content' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 4096)),
                )
            )
        ));
    }
}
