<?php
namespace NotifierServerClient\Requests\Messages;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use NotifierServerClient\Requests\Resources\ResourcesCreateRequest;
use NotifierServerClient\Types\CommandType;
use NotifierServerClient\Types\PriorityType;
use Symfony\Component\Validator\Constraints;

class MessagesCreateExtendedRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $messageId = '';

    /**
     * @var ResourcesCreateRequest
     */
    public $resource;

    /**
     * @var string
     */
    public $priority = '';

    /**
     * @var array
     */
    public $urlParams = array();

    /**
     * @var array
     */
    public $headers = array();

    /**
     * @var string
     */
    public $command = '';

    /**
     * @var string
     */
    public $content = '';

    /**
     * @var string
     */
    public $sendAt = '';

    /**
     * @var int
     */
    public $successHttpStatus;

    /**
     * @var string
     */
    public $successResponse = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var string
     */
    public $isSendNotReceivedNotify = '';

    /**
     * @param string $messageId
     * @param ResourcesCreateRequest $resource
     * @param string $priority
     * @param string $command
     * @param string $content
     * @param string $sendAt
     * @param int $successHttpStatus
     * @param string $successResponse
     * @param string $description
     * @param string $isSendNotReceivedNotify
     */
    public function __construct($messageId, ResourcesCreateRequest $resource, $urlParams = array(), $headers = array(),
                                $successHttpStatus = 201, $priority = PriorityType::MIDDLE,
                                $command = CommandType::CREATE, $content = "",
                                $successResponse = "",  $isSendNotReceivedNotify = true, $sendAt = "", $description = "")
    {
        $this->messageId = $messageId;
        $this->resource = $resource;
        $this->urlParams = $urlParams;
        $this->headers = $headers;
        $this->priority = $priority;
        $this->command = $command;
        $this->content = $content;
        $this->sendAt = $sendAt;
        $this->successHttpStatus = $successHttpStatus;
        $this->successResponse = $successResponse;
        $this->description = $description;
        $this->isSendNotReceivedNotify = $isSendNotReceivedNotify;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'messageId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 64)),
            ),
            'resource' => new Constraints\Collection(array(
                'url' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 1000)),
                    new Constraints\Url(),
                ),
                'description' => array(
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 0, 'max' => 100)),
                ),
                'setting' => new Constraints\Collection(array(
                    'settingId' => array(
                        new Constraints\NotNull(),
                        new Constraints\NotBlank(),
                        new Constraints\Type('string'),
                        new Constraints\Length(array('min' => 1, 'max' => 64)),
                    ),
                    'title' => array(
                        new Constraints\NotNull(),
                        new Constraints\NotBlank(),
                        new Constraints\Type('string'),
                        new Constraints\Length(array('min' => 1, 'max' => 100)),
                    ),
                    'count' => array(
                        new Constraints\Type('integer'),
                    ),
                    'intervals' => array(
                        new Constraints\Type('array'),
                        new Constraints\All(
                            new Constraints\Type('integer')
                        ),
                    ),
                    'timeout' => array(
                        new Constraints\Type('integer'),
                    ),
                    'callbackUrl' => array(
                        new Constraints\Type('string'),
                        new Constraints\Length(array('min' => 1, 'max' => 1000)),
                        new Constraints\Url(),
                    ),
                    'description' => array(
                        new Constraints\Type('string'),
                        new Constraints\Length(array('min' => 0, 'max' => 256)),
                    ),
                )),
            )),
            'priority' => array(
                new Constraints\Type('string'),
            ),
            'command' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
            ),
            'content' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 4096)),
            ),
            'sendAt' => array(
                new Constraints\Type('string'),
                new Constraints\DateTime(),
            ),
            'successHttpStatus' => array(
                new Constraints\Type('integer'),
            ),
            'successResponse' => array(
                new Constraints\Type('string'),
            ),
            'description' => array(
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 0, 'max' => 100)),
            ),
            'isSendNotReceivedNotify' => array(
                new Constraints\Type('boolean'),
            ),
        ));
    }
}
