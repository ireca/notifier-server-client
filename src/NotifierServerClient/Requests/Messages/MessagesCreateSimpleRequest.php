<?php
namespace NotifierServerClient\Requests\Messages;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use NotifierServerClient\Types\CommandType;
use NotifierServerClient\Types\PriorityType;
use Symfony\Component\Validator\Constraints;

class MessagesCreateSimpleRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $messageId = '';

    /**
     * @var string
     */
    public $resourceId = '';

    /**
     * @var array
     */
    public $urlParams = array();

    /**
     * @var array
     */
    public $headers = array();

    /**
     * @var string
     */
    public $priority = '';

    /**
     * @var string
     */
    public $command = '';

    /**
     * @var string
     */
    public $content = '';

    /**
     * @param string $messageId
     * @param string $resourceId
     * @param string $priority
     * @param string $command
     * @param string $content
     */
    public function __construct($messageId, $resourceId, $urlParams = array(), $headers = array(), $priority = PriorityType::MIDDLE,
                                $command = CommandType::CREATE, $content = "")
    {
        $this->messageId = $messageId;
        $this->resourceId = $resourceId;
        $this->urlParams = $urlParams;
        $this->headers = $headers;
        $this->priority = $priority;
        $this->command = $command;
        $this->content = $content;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'messageId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 64)),
            ),
            'resourceId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 64)),
            ),
            'priority' => array(
                new Constraints\Type('string'),
            ),
            'command' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
            ),
            'content' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 4096)),
            ),
        ));
    }
}
