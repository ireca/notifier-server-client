<?php
namespace NotifierServerClient\Requests\Messages;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;

class MessagesCreateSimpleListRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var array
     */
    private $elements = array();

    /**
     * @return Constraints\All
     */
    public function getConstraints()
    {
        return new Constraints\All(
            new Constraints\Collection(array(
                'messageId' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 64)),
                ),
                'resourceId' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 64)),
                ),
                'priority' => array(
                    new Constraints\Type('string'),
                ),
                'command' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                ),
                'content' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 4096)),
                ),
            ))
        );
    }

    /**
     * @param MessagesCreateSimpleRequest $element
     *
     * @return true
     */
    public function add(MessagesCreateSimpleRequest $element)
    {
        $this->elements[] = $element;

        return true;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return json_decode(json_encode($this->elements), true);
    }
}
