<?php
namespace NotifierServerClient\Requests\Messages;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;

class MessagesCreateExtendedListRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var array
     */
    private $elements = array();

    /**
     * @return Constraints\All
     */
    public function getConstraints()
    {
        return new Constraints\All(
            new Constraints\Collection(array(
                'messageId' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 64)),
                ),
                'resource' => new Constraints\Collection(array(
                    'url' => array(
                        new Constraints\NotNull(),
                        new Constraints\NotBlank(),
                        new Constraints\Type('string'),
                        new Constraints\Length(array('min' => 1, 'max' => 1000)),
                        new Constraints\Url(),
                    ),
                    'description' => array(
                        new Constraints\NotNull(),
                        new Constraints\NotBlank(),
                        new Constraints\Type('string'),
                        new Constraints\Length(array('min' => 1, 'max' => 100)),
                    ),
                    'setting' => new Constraints\Collection(array(
                        'settingId' => array(
                            new Constraints\NotNull(),
                            new Constraints\NotBlank(),
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 64)),
                        ),
                        'title' => array(
                            new Constraints\NotNull(),
                            new Constraints\NotBlank(),
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 100)),
                        ),
                        'count' => array(
                            new Constraints\Type('integer'),
                        ),
                        'intervals' => array(
                            new Constraints\Type('array'),
                            new Constraints\All(
                                new Constraints\Type('integer')
                            ),
                        ),
                        'timeout' => array(
                            new Constraints\Type('integer'),
                        ),
                        'callbackUrl' => array(
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 1000)),
                            new Constraints\Url(),
                        ),
                        'description' => array(
                            new Constraints\Type('string'),
                            new Constraints\Length(array('min' => 1, 'max' => 256)),
                        ),
                    )),
                )),
                'priority' => array(
                    new Constraints\Type('string'),
                ),
                'command' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                ),
                'content' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 4096)),
                ),
                'sendAt' => array(
                    new Constraints\Type('string'),
                    new Constraints\DateTime(),
                ),
                'successHttpStatus' => array(
                    new Constraints\Type('integer'),
                ),
                'successResponse' => array(
                    new Constraints\Type('string'),
                ),
                'description' => array(
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 100)),
                ),
                'isSendNotReceivedNotify' => array(
                    new Constraints\Type('boolean'),
                ),
            ))
        );
    }

    /**
     * @param MessagesCreateExtendedRequest $element
     * @return true
     */
    public function add(MessagesCreateExtendedRequest $element)
    {
        $this->elements[] = $element;

        return true;
    }

    /**
     * @return false|string
     */
    public function getJsonContent()
    {
        return json_encode($this->elements);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return json_decode(json_encode($this->elements), true);
    }
}
