<?php
namespace NotifierServerClient\Requests\DefaultSettings;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;

class DefaultSettingsCreateRequest extends AbstractBaseRequest implements RequestInterface
{

    /**
     * @var string
     */
    public $settingId = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var int
     */
    public $count;

    /**
     * @var array
     */
    public $intervals = array();

    /**
     * @var int
     */
    public $timeout;

    /**
     * @var string
     */
    public $callbackUrl = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @param string $settingId
     * @param string $title
     * @param int $count
     * @param array $intervals
     * @param int $timeout
     * @param string $callbackUrl
     * @param string $description
     */
    public function __construct($settingId, $title, $count, array $intervals, $timeout, $callbackUrl, $description)
    {
        $this->settingId = $settingId;
        $this->title = $title;
        $this->count = $count;
        $this->intervals = $intervals;
        $this->timeout = $timeout;
        $this->callbackUrl = $callbackUrl;
        $this->description = $description;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'settingId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 64)),
            ),
            'title' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 100)),
            ),
            'count' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('integer'),
            ),
            'intervals' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('array'),
                new Constraints\All(
                    new Constraints\Type('integer')
                ),
            ),
            'timeout' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('integer'),
            ),
            'callbackUrl' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Url(),
            ),
            'description' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 100)),
            ),
        ));
    }
}
