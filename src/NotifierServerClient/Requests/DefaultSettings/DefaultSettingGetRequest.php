<?php
namespace NotifierServerClient\Requests\DefaultSettings;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;

class DefaultSettingGetRequest extends AbstractBaseRequest implements RequestInterface
{

    /**
     * @var string
     */
    public $settingId = '';

    /**
     * @param string $settingId
     */
    public function __construct($settingId)
    {
        $this->settingId = $settingId;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'settingId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 64)),
            ),
        ));
    }
}
