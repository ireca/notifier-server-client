<?php
namespace NotifierServerClient\Requests\Resources;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;

class ResourcesDeleteRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $resourceId = '';

    /**
     * @param string $resourceId
     */
    public function __construct($resourceId)
    {
        $this->resourceId = $resourceId;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'resourceId' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 64)),
            ),
        ));
    }
}
