<?php
namespace NotifierServerClient\Requests\Resources;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;

class ResourcesGetRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var int
     */
    public $limit;

    /**
     * @var int
     */
    public $offset;

    /**
     * @param int $limit
     * @param int $offset
     */
    public function __construct($limit, $offset)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'limit' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('integer'),
            ),
            'offset' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('integer'),
            ),
        ));
    }
}
