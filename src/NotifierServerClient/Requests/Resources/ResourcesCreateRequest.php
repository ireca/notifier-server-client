<?php
namespace NotifierServerClient\Requests\Resources;

use NotifierServerClient\Requests\AbstractBaseRequest;
use NotifierServerClient\Requests\RequestInterface;
use Symfony\Component\Validator\Constraints;

class ResourcesCreateRequest extends AbstractBaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $url = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var SettingsRequest
     */
    public $setting;

    /**
     * @param string $url
     * @param string $description
     * @param SettingsRequest $setting
     */
    public function __construct($url, $description = '', SettingsRequest $setting = null)
    {
        $this->url = $url;
        $this->description = $description;
        $this->setting = $setting;
    }

    /**
     * @return Constraints\Collection
     */
    public function getConstraints()
    {
        return new Constraints\Collection(array(
            'url' => array(
                new Constraints\NotNull(),
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 1000)),
                new Constraints\Url(),
            ),
            'description' => array(
                new Constraints\Optional(),
                new Constraints\Type('string'),
                new Constraints\Length(array('min' => 1, 'max' => 100)),
            ),
            'setting' => new Constraints\Collection(array(
                'settingId' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 64)),
                ),
                'title' => array(
                    new Constraints\NotNull(),
                    new Constraints\NotBlank(),
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 100)),
                ),
                'count' => array(
                    new Constraints\Type('integer'),
                ),
                'intervals' => array(
                    new Constraints\Type('array'),
                    new Constraints\All(
                        new Constraints\Type('integer')
                    ),
                ),
                'timeout' => array(
                    new Constraints\Type('integer'),
                ),
                'callbackUrl' => array(
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 1000)),
                    new Constraints\Url(),
                ),
                'description' => array(
                    new Constraints\Type('string'),
                    new Constraints\Length(array('min' => 1, 'max' => 256)),
                ),
            )),
        ));
    }
}
