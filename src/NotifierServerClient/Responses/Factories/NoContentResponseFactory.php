<?php
namespace NotifierServerClient\Responses\Factories;

use NotifierServerClient\Responses\NoContentResponse;

class NoContentResponseFactory
{
    /**
     * @return NoContentResponse
     */
    public static function create()
    {
        return new NoContentResponse();
    }
}
