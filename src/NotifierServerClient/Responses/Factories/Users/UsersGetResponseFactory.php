<?php
namespace NotifierServerClient\Responses\Factories\Users;

use NotifierServerClient\Responses\Users\UsersGetResponse;

class UsersGetResponseFactory
{
    /**
     * @param array $response
     *
     * @return UsersGetResponse
     */
    public static function create(array $response)
    {
        $user = new UsersGetResponse();
        $user->setAttributes($response);

        return $user;
    }
}
