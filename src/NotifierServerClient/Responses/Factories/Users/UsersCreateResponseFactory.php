<?php
namespace NotifierServerClient\Responses\Factories\Users;

use NotifierServerClient\Responses\Users\UsersCreateResponse;

class UsersCreateResponseFactory
{
    /**
     * @param array $response
     *
     * @return UsersCreateResponse
     */
    public static function create(array $response)
    {
        $userCreate = new UsersCreateResponse();
        $userCreate->setAttributes($response);

        return $userCreate;
    }
}
