<?php
namespace NotifierServerClient\Responses\Factories\Users;

use NotifierServerClient\Responses\Users\UsersGetResponse;
use NotifierServerClient\Responses\Users\UsersGetListResponse;

class UsersGetListResponseFactory
{
    /**
     * @param array $response
     *
     * @return UsersGetListResponse
     */
    public static function create(array $response)
    {
        $listUsers = new UsersGetListResponse();
        foreach ($response as $attributes) {
            $user = new UsersGetResponse();
            $user->setAttributes($attributes);
            $listUsers->add($user);
        }

        return $listUsers;
    }
}
