<?php
namespace NotifierServerClient\Responses\Factories\DefaultSettings;

use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsResponse;
use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsListResponse;

class DefaultSettingsListResponseFactory
{
    /**
     * @param array $response
     *
     * @return DefaultSettingsListResponse
     */
    public static function create(array $response)
    {
        $defaultSettingsList = new DefaultSettingsListResponse();
        foreach ($response as $attributes) {
            $defaultSetting = new DefaultSettingsResponse();
            $defaultSetting->setAttributes($attributes);
            $defaultSettingsList->add($defaultSetting);
        }

        return $defaultSettingsList;
    }
}
