<?php
namespace NotifierServerClient\Responses\Factories\DefaultSettings;

use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsResponse;

class DefaultSettingsResponseFactory
{
    /**
     * @param array $response
     *
     * @return DefaultSettingsResponse
     */
    public static function create(array $response)
    {
        $defaultSetting = new DefaultSettingsResponse();
        $defaultSetting->setAttributes($response);

        return $defaultSetting;
    }
}
