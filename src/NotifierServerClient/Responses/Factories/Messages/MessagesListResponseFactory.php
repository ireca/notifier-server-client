<?php
namespace NotifierServerClient\Responses\Factories\Messages;

use NotifierServerClient\Responses\Messages\MessagesSimpleResponse;
use NotifierServerClient\Responses\Messages\MessagesListResponse;

class MessagesListResponseFactory
{
    /**
     * @param array $response
     *
     * @return MessagesListResponse
     */
    public static function create(array $response)
    {
        $messagesList = new MessagesListResponse();
        foreach ($response as $attributes) {
            $message = new MessagesSimpleResponse();
            $message->setAttributes($attributes);
            $messagesList->add($message);
        }

        return $messagesList;
    }
}
