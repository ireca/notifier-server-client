<?php
namespace NotifierServerClient\Responses\Factories\Messages;

use NotifierServerClient\Responses\Messages\MessagesExtendedResponse;

class MessagesExtendedResponseFactory
{
    public static function create(array $response)
    {
        $messageExtended = new MessagesExtendedResponse();
        $messageExtended->setAttributes($response);

        return $messageExtended;
    }
}
