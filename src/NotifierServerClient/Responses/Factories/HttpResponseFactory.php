<?php
namespace NotifierServerClient\Responses\Factories;

use GuzzleHttp\Psr7\Response;
use NotifierServerClient\Responses\NotifierServerResponse;

class HttpResponseFactory
{
    /**
     * @param Response $response
     *
     * @return NotifierServerResponse
     */
    public static function create(Response $response)
    {
        $body = $response->getBody();

        return new NotifierServerResponse($response->getStatusCode(), $body->getContents());
    }
}
