<?php
namespace NotifierServerClient\Responses\Factories\Callback;

use NotifierServerClient\Responses\Callback\CallbackListResponse;
use NotifierServerClient\Responses\Callback\CallbackResponse;

class CallbackListResponseFactory
{
    public static function create(array $response)
    {
        $callbackList = new CallbackListResponse();
        foreach ($response as $attributes) {
            $callback = new CallbackResponse();
            $callback->setAttributes($attributes);
            $callbackList->add($callback);
        }

        return $callbackList;
    }
}