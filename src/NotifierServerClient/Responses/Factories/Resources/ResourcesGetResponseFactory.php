<?php
namespace NotifierServerClient\Responses\Factories\Resources;

use NotifierServerClient\Responses\Resources\ResourcesGetResponse;

class ResourcesGetResponseFactory
{
    /**
     * @param array $response
     *
     * @return ResourcesGetResponse
     */
    public static function create(array $response)
    {
        $user = new ResourcesGetResponse();
        $user->setAttributes($response);

        return $user;
    }
}
