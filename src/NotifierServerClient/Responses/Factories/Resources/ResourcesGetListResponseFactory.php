<?php
namespace NotifierServerClient\Responses\Factories\Resources;

use NotifierServerClient\Responses\Resources\ResourcesGetResponse;
use NotifierServerClient\Responses\Resources\ResourcesGetListResponse;

class ResourcesGetListResponseFactory
{
    /**
     * @param array $response
     *
     * @return ResourcesGetListResponse
     */
    public static function create(array $response)
    {
        $listResources = new ResourcesGetListResponse();
        foreach ($response as $attributes) {
            $resource = new ResourcesGetResponse();
            $resource->setAttributes($attributes);
            $listResources->add($resource);
        }

        return $listResources;
    }
}
