<?php
namespace NotifierServerClient\Responses\Factories\Resources;

use NotifierServerClient\Responses\Resources\ResourcesCreateResponse;

class ResourceCreateResponseFactory
{
    /**
     * @param array $response
     *
     * @return ResourcesCreateResponse
     */
    public static function create(array $response)
    {
        $user = new ResourcesCreateResponse();
        $user->setAttributes($response);

        return $user;
    }
}
