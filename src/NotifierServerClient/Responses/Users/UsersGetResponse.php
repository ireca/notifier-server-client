<?php
namespace NotifierServerClient\Responses\Users;

use NotifierServerClient\Responses\AbstractBaseResponse;

class UsersGetResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $userId = '';

    /**
     * @var string
     */
    public $role = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $description = '';
}
