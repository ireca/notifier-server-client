<?php
namespace NotifierServerClient\Responses\Users;

use NotifierServerClient\Responses\AbstractBaseResponse;

class UsersCreateResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $token;
}
