<?php
namespace NotifierServerClient\Responses;

class NotifierServerResponse
{
    /**
     * @var integer
     */
    public $code = 0;

    /**
     * @var string
     */
    public $content = '';

    function __construct($code, $content)
    {
        $this->code = $code;
        $this->content = $content;
    }
}
