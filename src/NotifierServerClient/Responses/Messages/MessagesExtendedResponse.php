<?php
namespace NotifierServerClient\Responses\Messages;

use NotifierServerClient\Responses\AbstractBaseResponse;

class MessagesExtendedResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $messageId = '';

    /**
     * @var string
     */
    public $priority = '';

    /**
     * @var array
     */
    public $resource = array();

    /**
     * @var string
     */
    public $command = '';

    /**
     * @var string
     */
    public $content = '';

    /**
     * @var string
     */
    public $sendAt = '';

    /**
     * @var int
     */
    public $successHttpStatus;

    /**
     * @var string
     */
    public $successResponse = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var bool
     */
    public $isSendNotReceivedNotify;

    /**
     * @var bool
     */
    public $isSent;

    /**
     * @var int
     */
    public $attemptCount;

    /**
     * @var bool
     */
    public $isSentCallback;

    /**
     * @var int
     */
    public $callbackAttemptCount;

    /**
     * @var string
     */
    public $createdAt = '';
}
