<?php
namespace NotifierServerClient\Responses\Messages;

use NotifierServerClient\Responses\AbstractBaseResponse;

class MessagesSimpleResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $messageId = '';

    /**
     * @var string
     */
    public $resourceId = '';

    /**
     * @var int
     */
    public $code;

    /**
     * @var string
     */
    public $message = '';
}
