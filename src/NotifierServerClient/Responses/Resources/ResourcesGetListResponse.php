<?php
namespace NotifierServerClient\Responses\Resources;

use Doctrine\Common\Collections\ArrayCollection;

class ResourcesGetListResponse extends ArrayCollection
{
}
