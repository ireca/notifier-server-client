<?php
namespace NotifierServerClient\Responses\Resources;

class SettingResponse
{
    /**
     * @var int
     */
    public $count;

    /**
     * @var array
     */
    public $intervals = array();

    /**
     * @var int
     */
    public $timeout;

    /**
     * @var string
     */
    public $callbackUrl = '';
}
