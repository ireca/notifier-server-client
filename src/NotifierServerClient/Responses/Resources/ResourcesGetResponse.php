<?php
namespace NotifierServerClient\Responses\Resources;

use NotifierServerClient\Responses\AbstractBaseResponse;

class ResourcesGetResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $resourceId = '';

    /**
     * @var string
     */
    public $url = '';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var array
     */
    public $settings = array();
}
