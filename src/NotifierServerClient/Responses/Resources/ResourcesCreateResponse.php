<?php
namespace NotifierServerClient\Responses\Resources;

use NotifierServerClient\Responses\AbstractBaseResponse;

class ResourcesCreateResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $resourceId = '';
}
