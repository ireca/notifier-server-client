<?php
namespace NotifierServerClient\Responses\Callback;

use NotifierServerClient\Responses\AbstractBaseResponse;

class CallbackResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $messageId = '';

    /**
     * @var bool
     */
    public $isSent;
}
