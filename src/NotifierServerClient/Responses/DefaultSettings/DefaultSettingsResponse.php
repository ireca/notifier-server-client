<?php
namespace NotifierServerClient\Responses\DefaultSettings;

use NotifierServerClient\Responses\AbstractBaseResponse;

class DefaultSettingsResponse extends AbstractBaseResponse
{
    /**
     * @var string
     */
    public $settingId = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $callbackURL = '';

    /**
     * @var int
     */
    public $count;

    /**
     * @var array
     */
    public $intervals = array();

    /**
     * @var int
     */
    public $timeout;

    /**
     * @var string
     */
    public $description = '';
}
