<?php
namespace NotifierServerClient\Responses\DefaultSettings;

use Doctrine\Common\Collections\ArrayCollection;

class DefaultSettingsListResponse extends ArrayCollection
{
}
