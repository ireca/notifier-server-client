<?php

return array(
    'defaultSettings' => array(
        'setting' => 'api/v1/defaultSetting',
        'settings' => 'api/v1/defaultSettings',
        'create' => 'api/v1/defaultSettings',
        'update' => 'api/v1/defaultSettings',
        'delete' => 'api/v1/defaultSettings',
    ),
    'messages' => array(
        'message' => 'api/v1/message',
        'create' => 'api/v1/messages',
        'instantly_create' => 'api/v1/instantly/messages',
        'delete' => 'api/v1/messages',
    ),
    'resources' => array(
        'resource' => 'api/v1/resources',
        'resources' => 'api/v1/resources',
        'create' => 'api/v1/resources',
        'update' => 'api/v1/resources',
        'delete' => 'api/v1/resources',
    ),
    'users' => array(
        'user' => 'api/v1/user',
        'users' => 'api/v1/users',
        'create' => 'api/v1/users',
        'update' => 'api/v1/users',
        'delete' => 'api/v1/users',
    ),
);
