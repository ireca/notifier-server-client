<?php
use NotifierServerClient\Types\ProtocolType;
use Symfony\Component\Dotenv\Dotenv;

(new Dotenv())->load(dirname(__DIR__).'/.env');

return array(
    'notifierServerClient' => array(
        'requestClient' => ProtocolType::HTTP_REQUEST_SERVICE,
        'apiUrls' => require('apiUrls.php'),
        'callbackClientOptions' => require('callbackClientOptions.php'),
        'httpClientOptions' => require('httpClientOptions.php'),
        'restLogConfig' => require('restLog.php'),
        'slackNotifier' => require('slackNotifier.php'),
        'queueOptions' => array(
            'routingKey' => 'message',
            'headers' => array(
                'Authorization' => 'token',
            )
        ),
    )
);
