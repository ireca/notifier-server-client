<?php

return array(
    'baseUri' => isset($_ENV['BASE_URI']) ? $_ENV['BASE_URI'] : '',
    'timeout' => 1,
    'headers' => array(
        'Authorization' => isset($_ENV['AUTHORIZATION']) ? $_ENV['AUTHORIZATION'] : '',
        'Content-Type' => 'application/json'
    )
);
