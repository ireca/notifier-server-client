# Notifier Server Client - license management client

Notifier Server Client - The client part of the notification server. Allows you to create, update, delete, get users, default settings, messages, resources (in the future)

## Installation

Install the latest version with

```bash
$ composer require ireca/notifier-server-client
# from vendor/antonioo83/rest-log
$ composer install
#
# config/database-config.php Set up database connections in config
$ php vendor/doctrine/migrations/bin/doctrine-migrations.php migrations:migrate --configuration config/migrations-config.yml --db-configuration config/database-config.php
#
# create .env file in root and set config variables 
#
# BASE_URI='http://185.50.24.243:8081' - server url
# AUTHORIZATION='Bearer 14453f7cf1cda0c216f154aa07313b29' - Bearer auth
#
# DB settings for save logs
# DB_HOST='localhost' - DB host
# DB_PORT='3306' - DB port
# DB_USER='root' - DB user
# DB_PASSWORD='secret' - DB password
# DB_NAME='db_name' - DB name
#
# Slack settings fot notifications errors
# SLACK_NOTIFIER_SERVER='traktir-s3-service'
# SLACK_NOTIFIER_HOOK_URL='https://hooks.slack.com/services/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'
# SLACK_NOTIFIER_CHANEL='C3ZFNDS86'
$ touch .env
```

## Basic Usage

```php
<?php

require_once 'vendor/autoload.php';

use NotifierServerClient\Factories\Services\NotifierClientFactory;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingGetRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsCreateRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsDeleteRequest;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsGetRequest;
use NotifierServerClient\Requests\Messages\MessageGetRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateExtendedListRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateExtendedRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateSimpleListRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateSimpleRequest;
use NotifierServerClient\Requests\Messages\MessagesDeleteRequest;
use NotifierServerClient\Requests\Resources\ResourcesCreateRequest;
use NotifierServerClient\Requests\Resources\SettingsRequest;
use NotifierServerClient\Requests\Users\UserCreateRequest;
use NotifierServerClient\Requests\Users\UserDeleteRequest;
use NotifierServerClient\Requests\Users\UserGetRequest;
use NotifierServerClient\Requests\Users\UsersGetRequest;

$config = require('config/config.php');

$notifierClient = NotifierClientFactory::create($config);
```
### DEFAULT SETTINGS

#### Create default settings (Request example)
```php
$defaultSettingCreateRequest = new DefaultSettingsCreateRequest(
    '6371863a-cb98-4847-8d77-c9b1cb983ced-a',
    'Title: default setting for all resources',
    5,
    array(1, 3, 5, 7),
    5,
    'https://sitename.site/api/v1/orders/notify',
    'Description: default settings for all resources'
);
$data = $notifierClient->getDefaultSettingsApiService()->create($defaultSettingCreateRequest);
```
Response Example
```php
# NO CONTENT
```
Possible response codes:
```php
201 — successful request processing
400 — invalid request format
500 — internal server error
```

#### Update default settings (Request example)
```php
$defaultSettingUpdateRequest = new DefaultSettingsCreateRequest(
    '6371863a-cb98-4847-8d77-c9b1cb983ced-a',
    'Title: default setting for all resources',
    3,
    array(1, 3, 5, 7, 9),
    7,
    'https://sitename.site/api/v1/orders/notify',
    'Description: default settings for all resources'
);
$data = $notifierClient->getDefaultSettingsService()->update($defaultSettingCreateRequest);
```
Response Example
```php
# NO CONTENT
```
Possible response codes:
```php
204 — successful request processing
400 — invalid request format
500 — internal server error
```

#### Get default setting (Request example)
```php
$data = $notifierClient->getDefaultSettingsService()->getSetting('6371863a-cb98-4847-8d77-c9b1cb983ced-a');
```
Response Example
```php
DefaultSettingsResponse Object
(
    [settingId] => '6371863a-cb98-4847-8d77-c9b1cb983ced-a'
    [title] => 'Title: default setting for all resources'
    [callbackURL] => 'https://sitename.site/api/v1/orders/notify'
    [count] => 3
    [intervals] => Array
        (
            [0] => 1
            [1] => 5
            [2] => 20
        )
    [timeout] => 5
    [description] => 'Description: default settings for all resources'
)
```
Possible response codes:
```php
200 — successful request processing
400 — invalid request format
500 — internal server error
```
#### Get default settings (Request example)
```php
$data = $notifierClient->getDefaultSettingsService()->getSettings(10, 0);
```
Response Example
```php
DefaultSettingsListResponse Object
(
    [elements:ArrayCollection:private] => Array
        (
            [0] => DefaultSettingsResponse Object
                (
                    [settingId] => '6371863a-cb98-4847-8d77-c9b1cb983ced-a'
                    [title] => 'Title: default setting for all resources'
                    [callbackURL] => 'https://sitename.site/api/v1/orders/notify'
                    [count] => 3
                    [intervals] => Array
                        (
                            [0] => 1
                            [1] => 5
                            [2] => 20
                        )
                    [timeout] => 5
                    [description] => 'Description: default settings for all resources'
                )
            [1] => DefaultSettingsResponse Object
                (
                ........
                )
            [2] => DefaultSettingsResponse Object
                (
                ........
                )
        )
)
```
Possible response codes:
```php
200 — successful request processing
400 — invalid request format
500 — internal server error
```
#### Delete default settings (Request example)
```php
$data = $notifierClient->getDefaultSettingsService()->delete('6371863a-cb98-4847-8d77-c9b1cb983ced-a')
```
Response Example
```php
# NO CONTENT
```
Possible response codes:
```php
202 — successful request processing
400 — invalid request format
500 — internal server error
```

### MESSAGES

#### Create extend messages (Request example)
```php
$messagesListRequest = new MessagesCreateExtendedListRequest();
$messagesListRequest->add(new MessagesCreateExtendedRequest(
        '6371863a-cb98-4847-8d77-8',
        new ResourcesCreateRequest(
            'https://external.site/message',
            'Title: External resource',
            new SettingsRequest(
                '6371863a-cb98-4847-8d77-c9b1cb983ced-a',
                'Title: default setting for all resources',
                3,
                array(1, 3, 5),
                5,
                'https://sitename.site/api/v1/orders/notify',
                'Description: default settings for all resources'
            )
        ),
        'normal',
        'post',
        '{"message": "Happy New Year!"}',
        '2024-01-01 00:00:00',
        201,
        '{"resultCode": 0, "resultInfo": "OK"}',
        'desc',
        true
    )
);
$data = $notifierClient->getMessagesService()->create($messagesListRequest);
```
Response Example
```php
MessagesListResponse Object
(
    [elements:ArrayCollection:private] => Array
        (
            [0] => MessagesSimpleResponse Object
                (
                    [messageId] => '6371863a-cb98-4847-8d77-8'
                    [resourceId] => '5473544788677084164'
                    [code] => 0
                    [message] => 'Ok'
                )
        )
)

```
Possible response codes:
```php
201 — successful request processing
400 — invalid request format
500 — internal server error
```

#### Create simple messages (Request example)
```php
$messagesListRequest = new MessagesCreateSimpleListRequest();
$messagesListRequest->add(new MessagesCreateSimpleRequest(
        '6371863a-cb98-4847-8d77-8-1',
        '7255391982197163085',
        'normal',
        'post',
        '{"message": "Happy New Year!"}'
    )
);
$data = $notifierClient->getMessagesService()->create($messagesListRequest);
```
Response Example
```php
MessagesListResponse Object
(
    [elements:ArrayCollection:private] => Array
        (
            [0] => MessagesSimpleResponse Object
                (
                    [messageId] => '6371863a-cb98-4847-8d77-8-1'
                    [resourceId] => '7255391982197163085'
                    [code] => 0
                    [message] => 'Ok'
                )
        )
)

```
Possible response codes:
```php
201 — successful request processing
400 — invalid request format
500 — internal server error
```

#### Get message (Request example)
```php
$data = $notifierClient->getMessagesService()->getMessage('6371863a-cb98-4847-8d77-8');
```
Response Example
```php
MessagesExtendedResponse Object
(
    [messageId] => '6371863a-cb98-4847-8d77-8'
    [priority] => 'normal'
    [resource] => Array
        (
            [url] => 'https://external.site/message'
            [description] => 'External resource'
            [setting] => Array
                (
                    [settingId] => '6371863a-cb98-4847-8d77-c9b1cb983ced'
                    [title] => 'default setting for all resources'
                    [callbackURL] => 'https://api-courier.ireca.site/notify'
                    [count] => 3
                    [intervals] => Array
                        (
                            [0] => 1
                            [1] => 3
                            [2] => 5
                        )
                    [timeout] => 5
                    [description] => 'default settings'
                )

        )

    [command] => 'post'
    [content] => ''{"message": "Happy New Year!"}''
    [sendAt] => '2024-01-01 00:00:00'
    [successHttpStatus] => 201
    [successResponse] => '{"resultCode": 0, "resultInfo": "OK"}'
    [description] => 'description'
    [isSendNotReceivedNotify] => 
    [isSent] => 
    [attemptCount] => 4
    [isSentCallback] => 
    [callbackAttemptCount] => 0
    [createdAt] => '2023-05-25 14:27:10'
)
```
Possible response codes:
```php
200 — successful request processing
400 — invalid request format
500 — internal server error
```
#### Delete message (Request example)
```php
$data = $notifierClient->getMessagesService()->delete('6371863a-cb98-4847-8d77-8')
```
Response Example
```php
# NO CONTENT
```
Possible response codes:
```php
202 — successful request processing
400 — invalid request format
500 — internal server error
```
### Users

#### Create user (Request example)
```php
$userCreateRequest = new UserCreateRequest(
    '909fdfea-fff1-4dd5-97f7-612cf9840b-8',
    'service',
    'Test service users',
    'Test user to be deleted after tests'
);
$data = $notifierClient->getUserService()->create($userCreateRequest);
```
Response Example
```php
UsersCreateResponse Object
(
    [token] => b3e7b9bbdf734132827d7627001ba273
)
```
Possible response codes:
```php
201 — successful request processing
400 — invalid request format
500 — internal server error
```

#### Update user (Request example)
```php
$userUpdateRequest = new UserCreateRequest(
    '909fdfea-fff1-4dd5-97f7-612cf9840b-8',
    'service',
    'Test service users',
    'Test user to be deleted after tests'
);
$data = $notifierClient->getUserService()->update($userUpdateRequest);
```
Response Example
```php
# NO CONTENT
```
Possible response codes:
```php
204 — successful request processing
400 — invalid request format
500 — internal server error
```

#### Get user (Request example)
```php
$data = $notifierClient->getUserService()->getUser('909fdfea-fff1-4dd5-97f7-612cf9840b-8');
```
Response Example
```php
UsersGetResponse Object
(
    [userId] => '909fdfea-fff1-4dd5-97f7-612cf9840b-8'
    [role] => 'service'
    [title] => 'Test service users'
    [description] => 'Test user to be deleted after tests'
)

```
Possible response codes:
```php
200 — successful request processing
400 — invalid request format
500 — internal server error
```
#### Get default settings (Request example)
```php
$data = $notifierClient->getUserService()->getUsers(10, 0);
```
Response Example
```php
UsersGetListResponse Object
(
    [elements:ArrayCollection:private] => Array
        (
            [0] => UsersGetResponse Object
                (
                    [userId] => '909fdfea-fff1-4dd5-97f7-612cf9840b82'
                    [role] => 'admin'
                    [title] => 'Admin'
                    [description] => 'Admin'
                )

            [1] => UsersGetResponse Object
                (
                    ...
                )

            [2] => UsersGetResponse Object
                (
                    ...
                )
        )
)

```
Possible response codes:
```php
200 — successful request processing
400 — invalid request format
500 — internal server error
```
#### Delete user (Request example)
```php
$data = $notifierClient->getUserService()->delete('909fdfea-fff1-4dd5-97f7-612cf9840b82')
```
Response Example
```php
# NO CONTENT
```
Possible response codes:
```php
202 — successful request processing
400 — invalid request format
500 — internal server error
```

### Resources (not implemented)

#### Create resource (Request example)
```php
$resourcesCreateRequest = new ResourcesCreateRequest(
    'https://sitename.site/',
    'Description: External resource',
    array(
        3,
        array(1, 5, 20),
        5,
        'https://sitename.site/'
    )
);
$data = $notifierClient->getResourcesService()->create($resourcesCreateRequest);
```
Response Example
```php
ResourcesCreateResponse Object
(
    [resourceId] => '16aeccfad2dd46c6b19da440758eae28'
)
```
Possible response codes:
```php
201 — successful request processing
400 — invalid request format
500 — internal server error
```

#### Update resource (Request example)
```php
$resourcesUpdateRequest = new ResourcesCreateRequest(
    'https://sitename.site/',
    'Description: External resource',
    array(
        3,
        array(1, 5, 7),
        5,
        'https://sitename.site/'
    )
);
$data = $notifierClient->getResourcesService()->update($resourcesUpdateRequest);
```
Response Example
```php
# NO CONTENT
```
Possible response codes:
```php
204 — successful request processing
400 — invalid request format
500 — internal server error
```

#### Get resource (Request example)
```php
$data = $notifierClient->getResourcesService()->getResource('6371863a-cb98-4847-8d77-c9b1cb983ced-a');
```
Response Example
```php
ResourcesGetResponse Object
(
    [resourceId] => '6371863a-cb98-4847-8d77-c9b1cb983ced-a'
    [url] => 'https://sitename.site/api/v1/orders/notify'
    [description] => 'Description: default settings for all resources'
    [settings] => Array
        (
            [count] => 1
            [intervals] => array
                (
                    [0] => 1
                    [1] => 3
                    [2] => 5
                )
            [timeout] => 20
            [callbackUrl] => 'https://sitename.site/api/v1/orders/notify'
        )
)
```
Possible response codes:
```php
200 — successful request processing
400 — invalid request format
500 — internal server error
```
#### Get resources (Request example)
```php
$data = $notifierClient->getResourcesService()->getResources(10, 0);
```
Response Example
```php
ResourcesGetListResponse Object
(
    [elements:ArrayCollection:private] => Array
        (
            [0] => ResourcesGetResponse Object
                (
                    [resourceId] => '6371863a-cb98-4847-8d77-c9b1cb983ced-a'
                    [url] => 'https://sitename.site/api/v1/orders/notify'
                    [description] => 'Description: default settings for all resources'
                    [settings] => Array
                        (
                            [count] => 1
                            [intervals] => array
                                (
                                    [0] => 1
                                    [1] => 3
                                    [2] => 5
                                )
                            [timeout] => 20
                            [callbackUrl] => 'https://sitename.site/api/v1/orders/notify'
                        )
                )
            [1] => ResourcesGetResponse Object
                (
                ........
                )
            [2] => ResourcesGetResponse Object
                (
                ........
                )
        )
)
```
Possible response codes:
```php
200 — successful request processing
400 — invalid request format
500 — internal server error
```
#### Delete resource (Request example)
```php
$data = $notifierClient->getResourcesService()->delete('6371863a-cb98-4847-8d77-c9b1cb983ced-a')
```
Response Example
```php
# NO CONTENT
```
Possible response codes:
```php
202 — successful request processing
400 — invalid request format
500 — internal server error
```


### Requirements

S3 Storage Service works with

- PHP 5.5 or above
- Mysql 5.7 or above

### Submitting bugs and feature requests

Bugs and feature request are tracked on [GitLab](https://gitlab.com/dashboard/issues?assignee_username=eye-of-dev)

### Tests

php vendor/bin/codecept run unit

### License

Notifier Server Client is licensed under the MIT License - see the `LICENSE` file for details
