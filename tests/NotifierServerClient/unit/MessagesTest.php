<?php
namespace NotifierServerClient\unit;

use Codeception\Test\Unit;
use Doctrine\ORM\ORMException;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Factories\Services\NotifierClientFactory;
use NotifierServerClient\Requests\Messages\MessagesCreateExtendedListRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateExtendedRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateSimpleListRequest;
use NotifierServerClient\Requests\Messages\MessagesCreateSimpleRequest;
use NotifierServerClient\Requests\Resources\ResourcesCreateRequest;
use NotifierServerClient\Requests\Resources\SettingsRequest;
use NotifierServerClient\Responses\Messages\MessagesExtendedResponse;
use NotifierServerClient\Responses\Messages\MessagesListResponse;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Services\NotifierClientService;
use NotifierServerClient\UnitTester;

class MessagesTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var NotifierClientService
     */
    private $notifierClient;

    /**
     * @var string
     */
    private $messageExtendedId = '6371863a-cb98-4847-8d77-8';

    /**
     * @var string
     */
    private $messageSimpleId = '6371863a-cb98-4847-8d77-8-1';

    /**
     * @return void
     *
     * @throws ORMException
     * @throws NotifierClientException
     */
    protected function _before()
    {
        $config = require(dirname(dirname(dirname(__DIR__))).'/config/config.php');
        $this->notifierClient = NotifierClientFactory::create($config);
    }

    protected function _after()
    {
    }

    /**
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testCreateExtended()
    {
        $messagesListRequest = new MessagesCreateExtendedListRequest();
        $messagesListRequest->add(new MessagesCreateExtendedRequest(
                $this->getMessageExtendedId(),
                new ResourcesCreateRequest(
                    'https://external.site/message',
                    'External resource',
                    new SettingsRequest(
                        '6371863a-cb98-4847-8d77-c9b1cb983ced',
                        'default setting for all resources',
                        3,
                        array(1, 3, 5),
                        5,
                        'https://api-courier.ireca.site/notify',
                        'default settings'
                    )
                ),
                'normal',
                'post',
                '{"message": "Happy New Year!"}',
                '2023-05-31 00:00:00',
                201,
                '{"resultCode": 0, "resultInfo": "OK"}',
                'desc',
                true
            )
        );

        $data = $this->notifierClient->getMessagesService()->create($messagesListRequest);
        $this->assertInstanceOf(MessagesListResponse::class, $data);
    }

    /**
     * @depends testCreateExtended
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testCreateSimple()
    {
        $messagesListRequest = new MessagesCreateSimpleListRequest();
        $messagesListRequest->add(new MessagesCreateSimpleRequest(
            $this->getMessageSimpleId(),
            '7255391982197163085',
            'normal',
            'post',
            '{"message": "Happy New Year!"}'
        ));

        $data = $this->notifierClient->getMessagesService()->create($messagesListRequest);
        $this->assertInstanceOf(MessagesListResponse::class, $data);
    }

    /**
     * @depends testCreateExtended
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testGetMessage()
    {
        $data = $this->notifierClient->getMessagesService()->getMessage($this->getMessageExtendedId());
        $this->assertInstanceOf(MessagesExtendedResponse::class, $data);
    }

    /**
     * @depends testGetMessage
     * @depends testCreateSimple
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testDelete()
    {
        $data = $this->notifierClient->getMessagesService()->delete($this->getMessageExtendedId());
        $this->assertInstanceOf(NoContentResponse::class, $data);

        $data = $this->notifierClient->getMessagesService()->delete($this->getMessageSimpleId());
        $this->assertInstanceOf(NoContentResponse::class, $data);
    }

    /**
     * @return string
     */
    private function getMessageExtendedId()
    {
        return $this->messageExtendedId;
    }

    /**
     * @return string
     */
    private function getMessageSimpleId()
    {
        return $this->messageSimpleId;
    }
}
