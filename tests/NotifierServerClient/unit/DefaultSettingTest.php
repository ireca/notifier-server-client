<?php
namespace NotifierServerClient\unit;

use Codeception\Test\Unit;
use Doctrine\ORM\ORMException;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Factories\Services\NotifierClientFactory;
use NotifierServerClient\Requests\DefaultSettings\DefaultSettingsCreateRequest;
use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsListResponse;
use NotifierServerClient\Responses\DefaultSettings\DefaultSettingsResponse;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Services\NotifierClientService;
use NotifierServerClient\UnitTester;

class DefaultSettingTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var NotifierClientService
     */
    private $notifierClient;

    /**
     * @var string
     */
    private $settingId = '6371863a-cb98-4847-8d77-c9b1cb983ced-a';

    /**
     * @return void
     *
     * @throws ORMException
     * @throws NotifierClientException
     */
    protected function _before()
    {
        $config = require(dirname(dirname(dirname(__DIR__))).'/config/config.php');

        $this->notifierClient = NotifierClientFactory::create($config);
    }

    protected function _after()
    {
    }

    /**
     * @return void
     *
     * @throws NotifierClientException
     *
     */
    public function testCreate()
    {
        $defaultSettingsCreateRequest = new DefaultSettingsCreateRequest(
            $this->getSettingId(),
            'default setting for all resources',
            5,
            array(1, 3, 5),
            5,
            'https://api-courie.ireca.site/api/v1/orders/notify',
            'default settings'
        );

        $data = $this->notifierClient->getDefaultSettingsService()->create($defaultSettingsCreateRequest);
        $this->assertInstanceOf(NoContentResponse::class, $data);
    }

    /**
     * @depends testCreate
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testUpdate()
    {
        $defaultSettingsCreateRequest = new DefaultSettingsCreateRequest(
            $this->getSettingId(),
            'default setting for all resources',
            2,
            array(1, 5, 9),
            7,
            'https://api-courie.ireca.site/api/v1/orders/notify',
            'default settings'
        );

        $data = $this->notifierClient->getDefaultSettingsService()->update($defaultSettingsCreateRequest);
        $this->assertInstanceOf(NoContentResponse::class, $data);
    }

    /**
     * @depends testCreate
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testGetSetting()
    {
        $data = $this->notifierClient->getDefaultSettingsService()->getSetting($this->getSettingId());
        $this->assertInstanceOf(DefaultSettingsResponse::class, $data);
    }

    /**
     * @depends testCreate
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testGetSettingsList()
    {
        $data = $this->notifierClient->getDefaultSettingsService()->getSettings(10, 0);
        $this->assertInstanceOf(DefaultSettingsListResponse::class, $data);
    }

    /**
     * @depends testUpdate
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testDelete()
    {
        $data = $this->notifierClient->getDefaultSettingsService()->delete($this->getSettingId());
        $this->assertInstanceOf(NoContentResponse::class, $data);
    }

    /**
     * @return string
     */
    private function getSettingId()
    {
        return $this->settingId;
    }
}
