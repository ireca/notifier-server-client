<?php
namespace NotifierServerClient\unit;

use Codeception\Test\Unit;
use Doctrine\ORM\ORMException;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Factories\Services\NotifierClientFactory;
use NotifierServerClient\Requests\Users\UserCreateRequest;
use NotifierServerClient\Requests\Users\UserDeleteRequest;
use NotifierServerClient\Requests\Users\UserGetRequest;
use NotifierServerClient\Requests\Users\UsersGetRequest;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Responses\Users\UsersGetResponse;
use NotifierServerClient\Responses\Users\UsersCreateResponse;
use NotifierServerClient\Responses\Users\UsersGetListResponse;
use NotifierServerClient\Services\NotifierClientService;
use NotifierServerClient\UnitTester;

class UserTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var NotifierClientService
     */
    private $notifierClient;

    /**
     * @var string
     */
    private $userId = '909fdfea-fff1-4dd5-97f7-612cf9840b-8';

    /**
     * @return void
     *
     * @throws ORMException
     * @throws NotifierClientException
     */
    protected function _before()
    {
        $config = require(dirname(dirname(dirname(__DIR__))).'/config/config.php');

        $this->notifierClient = NotifierClientFactory::create($config);
    }

    protected function _after()
    {
    }

    /**
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testCreate()
    {
        $userCreateRequest = new UserCreateRequest(
            $this->getUserId(),
            'service',
            'Test service users',
            'Test user to be deleted after tests'
        );

        $data = $this->notifierClient->getUserService()->create($userCreateRequest);
        $this->assertInstanceOf(UsersCreateResponse::class, $data);
    }

    /**
     * @depends testCreate
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testUpdate()
    {
        $userPutRequest = new UserCreateRequest(
            $this->getUserId(),
            'service',
            'Test service users',
            'Test user to be deleted after tests'
        );
        $data = $this->notifierClient->getUserService()->update($userPutRequest);
        $this->assertInstanceOf(NoContentResponse::class, $data);
    }

    /**
     * @depends testCreate
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testGetUser()
    {
        $data = $this->notifierClient->getUserService()->getUser($this->getUserId());
        $this->assertInstanceOf(UsersGetResponse::class, $data);
    }

    /**
     * @depends testCreate
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testGetUsersList()
    {
        $data = $this->notifierClient->getUserService()->getUsers(1000, 0);
        $this->assertInstanceOf(UsersGetListResponse::class, $data);
    }

    /**
     * @depends testUpdate
     *
     * @return void
     *
     * @throws NotifierClientException
     */
    public function testDelete()
    {
        $data = $this->notifierClient->getUserService()->delete($this->getUserId());
        $this->assertInstanceOf(NoContentResponse::class, $data);
    }

    /**
     * @return string
     */
    private function getUserId()
    {
        return $this->userId;
    }
}
