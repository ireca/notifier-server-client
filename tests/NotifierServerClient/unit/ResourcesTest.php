<?php
namespace NotifierServerClient\unit;

use Codeception\Test\Unit;
use Doctrine\ORM\ORMException;
use NotifierServerClient\Exceptions\NotifierClientException;
use NotifierServerClient\Factories\Services\NotifierClientFactory;
use NotifierServerClient\Requests\Resources\ResourceGetRequest;
use NotifierServerClient\Requests\Resources\ResourcesCreateRequest;
use NotifierServerClient\Requests\Resources\ResourcesDeleteRequest;
use NotifierServerClient\Requests\Resources\ResourcesGetRequest;
use NotifierServerClient\Responses\NoContentResponse;
use NotifierServerClient\Responses\Resources\ResourcesCreateResponse;
use NotifierServerClient\Responses\Resources\ResourcesGetListResponse;
use NotifierServerClient\Responses\Resources\ResourcesGetResponse;
use NotifierServerClient\Services\NotifierClientService;
use NotifierServerClient\UnitTester;

class ResourcesTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @var NotifierClientService
     */
    private $notifierClient;

    /**
     * @return void
     *
     * @throws ORMException
     *
     * @throws NotifierClientException
     */
    protected function _before()
    {
        $config = require(dirname(dirname(dirname(__DIR__))).'/config/config.php');

        $this->notifierClient = NotifierClientFactory::create($config);
    }

    protected function _after()
    {
    }

    /**
     * @return void
     */
    public function testCreate()
    {
//        $resourcesCreateRequest = new ResourcesCreateRequest();
//        $data = $this->notifierClient->getResourcesService()->create($resourcesCreateRequest);
//
//        $this->assertInstanceOf(ResourcesCreateResponse::class, $data);
    }

    /**
     * @depends testCreate
     *
     * @return void
     */
    public function testUpdate()
    {
//        $resourcesUpdateRequest = new ResourcesCreateRequest();
//        $data = $this->notifierClient->getResourcesService()->update($resourcesUpdateRequest);
//
//        $this->assertInstanceOf(NoContentResponse::class, $data);
    }

    /**
     * @depends testUpdate
     *
     * @return void
     */
    public function testGetResource()
    {
//        $resourceGetRequest = new ResourceGetRequest();
//        $data = $this->notifierClient->getResourcesService()->getResource($resourceGetRequest);
//
//        $this->assertInstanceOf(ResourcesGetResponse::class, $data);
    }

    /**
     * @depends testUpdate
     *
     * @return void
     */
    public function testGetResourcesList()
    {
//        $resourcesGetRequest = new ResourcesGetRequest(100, 0);
//        $data = $this->notifierClient->getResourcesService()->getResources($resourcesGetRequest);
//
//        $this->assertInstanceOf(ResourcesGetListResponse::class, $data);
    }

    /**
     * @depends testUpdate
     *
     * @return void
     */
    public function testDelete()
    {
//        $resourcesDeleteRequest = new ResourcesDeleteRequest();
//        $data = $this->notifierClient->getResourcesService()->delete($resourcesDeleteRequest);
//
//        $this->assertInstanceOf(NoContentResponse::class, $data);
    }
}
